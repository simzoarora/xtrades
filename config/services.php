<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, SparkPost and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],
    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],
    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_REDIRECT'),
    ],
    'discord' => [
        'client_id' => env('DISCORD_CLIENT_ID'),
        'client_secret' => env('DISCORD_CLIENT_SECRET'),
        'redirect' => env('DISCORD_REDIRECT')
    ],
    'paypal' => [
        'mode' => env('PAYPAL_MODE'),
        'username' => env('PAYPAL_API_USERNAME'),
        'password' => env('PAYPAL_API_PASSWORD'),
        'signature' => env('PAYPAL_API_SIGNATURE')
    ],
    'stripe_test' => [
        'key_1' => env('STRIPE_TEST_KEY_1'),
        'key_2' => env('STRIPE_TEST_KEY_2')
    ],
    'provider' => [
        'discord' => 'discord',
        'google' => 'google',
        'facebook' => 'facebook',
    ],
    'plans' => [
        'month' => 2,
        'year' => 5,
        'lifetime' => 6,
    ],
    'plans_ordering' => [
        'month' => 1,
        'year' => 2,
        'lifetime' => 3,
    ],
    'plans_ordering_reverse' => [
        '1' => 'month',
        '2' => 'year',
        '3' => 'lifetime',
    ],
    'plans_name' => [
        'month' => 'month',
        'year' => 'year',
        'lifetime' => 'lifetime',
    ],
    'status' => [
        'stripe' => 'active',
        'paypal' => 'Completed'
    ],
    'stripe_status' => [
        'active' => 'active',
        'pending' => 'pending',
        'paid' => 'paid',
        'succeeded' => 'succeeded',
        'incomplete' => 'incomplete'
    ],
    'stripe_webhook' => [
        'success' => 'invoice.payment_succeeded',
        'failed' => 'invoice.payment_failed',
        'charge_failed' => 'charge.failed'
    ],
    'paypal_webhook' => [
        'success' => 'recurring_payment',
        'failed' => 'recurring_payment_failed'
    ],
    'stripe_webhook_type' => [
        'invoice' => 'invoice'
    ],
    'subscribable_type' => [
        'stripe' => 'subscription_stripe',
        'paypal' => 'subscription_paypal',
        'manual' => 'subscription_manual'
    ],
    'subscribable_type_inverse' => [
        'subscription_stripe' => 'Stripe',
        'subscription_paypal' => 'Paypal',
        'subscription_manual' => 'Manual'
    ],
    'user_roles' => [
        0 => 'User',
        1 => 'Super Admin',
        2 => 'Admin',
        3 => 'Moderator',
        4 => 'Analyst',
        5 => 'Mentor',
        6 => 'Marketer'
    ],
    'user_roles_reverse' => [
        'user' => 0,
        'super_admin' => 1,
        'admin' => 2,
        'moderator' => 3,
        'analyst' => 4,
        'mentor' => 5,
        'marketer' => 6
    ],
    'payment_status' => [
        'paid' => 'Paid',
        'not_paid' => 'Not Paid'
    ],
    'discord_response' => [
        'confirmed' => 1,
        'un_confirmed' => 0
    ],
    'user_logs' => [
        'referral_points' => 'Referral Points',
        'credit' => 'credit',
        'debit' => 'debit',
    ],
    'currency_symbol' => [
        'dollar' => '$'
    ],
    'coupon_type' => [
        'stripe' => 1,
        'paypal' => 2
    ],
    'commision_rate' => [
        0 => ['points' => '1', 'percent' => '15%', 'level' => 1],
        1 => ['points' => '25', 'percent' => '18%', 'level' => 2],
        2 => ['points' => '50', 'percent' => '21%', 'level' => 3],
        3 => ['points' => '100', 'percent' => '24%', 'level' => 4],
        4 => ['points' => '200', 'percent' => '27%', 'level' => 5],
        5 => ['points' => '400', 'percent' => '30%', 'level' => 6],
        6 => ['points' => '1000', 'percent' => '40%', 'level' => 7],
    ],
    'user_points' => [
        'signup_points' => 1,
        'payment_points' => 5
    ],
    'point_type' => [
        'credit' => 1,
        'debit' => 2
    ],
    'point_source' => [
        'signup' => 1,
        'payment' => 2
    ],
    'payment_type' => [
        'stripe' => 1,
        'paypal' => 2
    ],
    'cancellation_status' => [
        'canceled' => 'canceled',
    ]
];
