<?php
use App\PayPalService;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('join/{affiliateId}', function ($affiliateId) {

    return view('storage', [
        'storage' => [
            '_rid' => $affiliateId
        ]
    ]);
});

Route::get('discord', function () {
    $guild = config('guild');
    return redirect()->to($guild['join_chat_redirect']);
});

Route::get('login/{driver}', 'LoginController@redirectToProvider')
    ->where('driver', 'google|discord|facebook');

Route::get('login/{driver}/callback', 'LoginController@handleProviderCallback')
    ->where('driver', 'google|discord|facebook');

Route::get('{any}', 'AppController@index')
    ->where('any', '^(?!api).*')
    ->name('app');
