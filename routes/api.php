<?php

use Illuminate\Http\Request;
use App\Http\Middleware\DiscordAuthentication;
use App\Http\Middleware\VerifyStripeSignature;
use App\Http\Middleware\VerifyPaypalSignature;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/{provider}', 'LoginController@authWithProviderToken');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/register', 'Auth\RegisterController@register');

Route::post('/web-hook', 'PlanController@webHook')->middleware(VerifyStripeSignature::class)->name('web-hook');

Route::post('/paypal-web-hook', 'PlanController@paypalWebHook')->middleware(VerifyPaypalSignature::class)->name('paypal-web-hook');

//user routes
Route::middleware('auth:api')->group(function () {
    Route::get('/users/@me', function (Request $request) {

        $user = $request->user();
        $user->load('subscriptions', 'connections');

        return $user;
    });

    Route::get('/users/@me/referrals', 'PlanController@fetchSubscriptions');

    Route::post('/users/@me/connections/{provider}', 'ConnectionController@addConnection')
        ->where('driver', 'google|discord|facebook');
    
    Route::get('/fetch-users', 'PlanController@fetchUsers');

    Route::get('/chart-data', 'PlanController@chartData');
});

//admin routes
Route::middleware('auth:api')->prefix('admin')->group(function () {
    Route::get('/fetch-admin-users', 'AdminController@fetchAdminUsers');
    Route::get('/fetch-discord-users', 'AdminController@fetchPaidUsers');
    
    Route::post('/update-profile', 'AdminController@updateProfile');
    Route::post('/update-role', 'AdminController@updateRole');
    
    Route::get('/fetch-plan', 'AdminController@fetchPlan');
    Route::post('/add-plan', 'AdminController@addPlan');
    
    Route::get('/fetch-transactions-details/{user_id}', 'AdminController@fetchTransactionsDetails');
    Route::post('/add-coupon', 'AdminController@addCoupon');
    Route::get('/fetch-coupons', 'AdminController@fetchCoupons');
});

//discord api authentication

Route::middleware(DiscordAuthentication::class)->group(function () {
    Route::get('/discord-change-roles', 'PlanController@discordPaidUsers');
    Route::get('/confirm-change/{changeId}/{discordId}', 'PlanController@confirmChange');
});

Route::post('/check-coupon', 'PlanController@checkCoupon');
Route::post('/check-coupon-paypal', 'PlanController@checkCouponPaypal');
Route::post('/plans/{plan}/stripe', 'PlanController@doStripe');
Route::post('/plans/{plan}/paypal', 'PlanController@getExpressCheckoutUrl');
Route::get('/plans/{plan}/paypal/{coupon_id}/{ref_id}/process', 'PlanController@doPayPal');
Route::get('/user-plan/{discord_id}', 'PlanController@userPlan');
Route::get('/discord-roles/{discord_id}', 'PlanController@discordRoles');
Route::post('/cancel-subscription', 'PlanController@cancelSubscription');