<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsConfirmedToUserProviderAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_provider_accounts', function (Blueprint $table) {
            $table->tinyInteger('is_confirm')->after('avatar')->default(0)->comment('0: Yes, 1: No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_provider_accounts', function (Blueprint $table) {
            $table->dropColumn('is_confirm');
        });
    }
}
