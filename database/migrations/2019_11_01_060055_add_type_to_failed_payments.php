<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToFailedPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('failed_payments', function (Blueprint $table) {
            $table->tinyInteger('type')->after('amount_remaining')->comment('1: Stripe, 2: Paypal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('failed_payments', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
