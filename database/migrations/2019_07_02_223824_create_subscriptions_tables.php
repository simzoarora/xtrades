<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscribable_id');
            $table->string('subscribable_type');

            $table->bigInteger('user_id');
            $table->bigInteger('plan_id');

            $table->string('status');
            $table->timestamp('synced_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    
        Schema::create('subscriptions_paypal', function ($table) {
            $table->increments('id');
            $table->string('profile_id');
            $table->string('status');
            $table->timestamp('paid_through')->nullable();
            $table->timestamp('synced_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('subscriptions_stripe', function ($table) {
            $table->increments('id');
            $table->string('subscription_id');
            $table->string('status');
            $table->timestamp('paid_through')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions_stripe');
        Schema::dropIfExists('subscriptions_paypal');
        Schema::dropIfExists('subscriptions');
    }
}
