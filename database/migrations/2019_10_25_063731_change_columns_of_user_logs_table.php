<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeColumnsOfUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //truncate table first
        DB::table('user_logs')->truncate();

        Schema::table('user_logs', function (Blueprint $table) {
            $table->dropColumn('logs');
            
            $table->tinyInteger('type')->after('user_id')->nullable()->comment('1: credit, 2: debit');
            $table->integer('points')->after('type')->nullable();
            
            $table->unsignedBigInteger('refer_id')->after('points')->nullable();
            $table->foreign('refer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //truncate table first
        DB::table('user_logs')->truncate();

        Schema::table('user_logs', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('points');

            $table->json('logs')->after('user_id')->nullable();

            $table->dropForeign(['refer_id']);
            $table->dropColumn('refer_id');
        });
    }
}
