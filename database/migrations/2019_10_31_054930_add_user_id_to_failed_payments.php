<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToFailedPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('failed_payments', function (Blueprint $table) {
            $table->dropColumn('customer_email');

            $table->unsignedBigInteger('user_id')->after('subscription_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('invoice_id')->after('customer_id')->nullable();
            $table->double('amount_due', 8, 2)->after('invoice_id')->nullable();
            $table->double('amount_paid', 8, 2)->after('amount_due')->nullable();
            $table->double('amount_remaining', 8, 2)->after('amount_paid')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('failed_payments', function (Blueprint $table) {
            $table->string('customer_email')->after('customer_id')->nullable();

            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');

            $table->dropColumn('invoice_id');
            $table->dropColumn('amount_due');
            $table->dropColumn('amount_paid');
            $table->dropColumn('amount_remaining');

        });
    }
}
