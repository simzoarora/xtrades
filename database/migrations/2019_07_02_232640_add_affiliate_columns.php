<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliateColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('affiliate_id');
            $table->string('referral_id')->nullable();
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('referral_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn(['referral_id']);
        });
         
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['affiliate_id', 'referral_id']);
        });
    }
}
