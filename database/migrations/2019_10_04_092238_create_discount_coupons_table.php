<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_coupons', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('coupon_name', 50)->nullable();
            $table->string('coupon_code', 10)->nullable();
            $table->tinyInteger('coupon_type')->comment('1: Stripe, 2: Paypal')->nullable();
            $table->tinyInteger('max_redemptions')->nullable();
            $table->double('discount_amount', 8, 2)->nullable();
            $table->double('discount_percent', 8, 2)->nullable();
            $table->dateTimeTz('redeem_by')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupons');
    }
}
