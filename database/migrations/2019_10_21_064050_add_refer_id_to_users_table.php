<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;
use App\User;

class AddReferIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('refer_id')->after('stripe_customer_id')->nullable();
            $table->foreign('refer_id')->references('id')->on('users');
        });

        // This will add foreign key value in refer_id database field for those user who joined site using referral link 
        $getAllUsers = User::with('referrals')->get();

        foreach ($getAllUsers as $user) {
            if ($user->referrals->count() > 0) {
                foreach ($user->referrals as $referrals) {
                    if (User::whereId($referrals->id)->update(['refer_id' => $user->id])) {
                        Log::info('Refer ID updated for ' . $referrals->id);
                    } else {
                        Log::error('Got error in updating Refer ID for ' . $referrals->id);
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['refer_id']);
            $table->dropColumn('refer_id');
        });
    }
}
