<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;
use App\User;

class AddPaymentIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('payment_id')->after('refer_id')->nullable();
            $table->foreign('payment_id')->references('id')->on('users');
        });

        // This will add foreign key value in payment_id database field for those user who joined site using referral link paid as well 
        $getAllUsers = User::with('referrals')->get();

        foreach ($getAllUsers as $user) {
            if ($user->referrals->count() > 0) {
                foreach ($user->referrals as $referrals) {
                    if (User::whereId($referrals->id)->update(['payment_id' => $user->id])) {
                        Log::info('Payment ID updated for ' . $referrals->id);
                    } else {
                        Log::error('Got error in updating Payment ID for ' . $referrals->id);
                    }
                }
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['payment_id']);
            $table->dropColumn('payment_id');
        });
    }
}
