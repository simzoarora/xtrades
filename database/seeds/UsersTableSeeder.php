<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Istrator',
            'email' => 'admin@admin.com',
            'user_role' => 1,
            'password' => bcrypt('secret'),
            'avail_points' => NULL,
            'last_login_at' => Carbon::now(),
            'affiliate_id' => Str::random(10),
            'created_at' => Carbon::now(),
        ]);
    }
}
