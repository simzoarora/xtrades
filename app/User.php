<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Events\UserCreating;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'referral_id', 'refer_id', 'avatar', 'stripe_customer_id','user_role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'referral_affiliate_id', 'ip_address'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
    ];

    /**
     * Override the default boot method to register some extra stuff.
     */
    protected static function boot()
    {
        static::saving(function ($model) {
            if (!$model->affiliate_id) {
                $model->affiliate_id = str_random(10);
            }
        });

        parent::boot();
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
    
    public function latestSubscriptions()
    {
        return $this->hasOne(Subscription::class)->latest();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function connections()
    {
        return $this->hasMany(UserProviderAccount::class);
    }
    
    public function referrals()
    {
        return $this->hasMany($this, 'refer_id');
    }
    
    public function subscription()
    {
        return $this->hasMany(Subscription::class);
    }
    
    public function bonusEarned()
    {
        return $this->hasMany(AmountTransferedByAdmin::class);
    }
}
