<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionStripe extends Model
{
    use SoftDeletes;

    protected $table = 'subscriptions_stripe';
    protected $dates = ['paid_through'];
    protected $fillable = ['subscription_id', 'status', 'paid_through'];

    public function subscription()
    {
        return $this->morphOne(Subscription::class, 'subscribable');
    }
}
