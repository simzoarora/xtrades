<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCoupon extends Model
{
    public function appliedSubscription()
    {
        return $this->hasMany('App\Subscription', 'coupon_id');
    }
}
