<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\DataAnalysisService;
use App\User;
use App\UserProviderAccount;
use App\Subscription;
use App\SubscriptionManual;
use App\Plan;
use App\DiscountCoupon;

class AdminController extends Controller
{

    /**
     * fetch referral leaderboard users for admin
     * @return type json
     */
    public function fetchAdminUsers()
    {
        $dataAnalys = new DataAnalysisService;

        $data = $dataAnalys->calculateAllValues(null, null);

        return response()->json($data);
    }

    /**
     * return all paid and discord users
     */
    public function fetchPaidUsers()
    {
        $getUsers = UserProviderAccount::with(['user.latestSubscriptions' => function ($query) {
            $query->checkActivePayment();
        }])->providerDiscord()->get();

        $data = array();
        foreach ($getUsers as $key => $user) {
            $latestSubscription = $user->user->latestSubscriptions;

            if (!empty($latestSubscription && $latestSubscription->subscribable->paid_through > Carbon::now())) {
                //calculate paid user details
                list($amount, $pStatus, $addPlan, $pMode, $notes) = $this->_calculatePaidUserDetails($latestSubscription);

                $start = Carbon::parse($latestSubscription->subscribable->created_at)->format('d M Y');
                $end = Carbon::parse($latestSubscription->subscribable->paid_through)->format('d M Y');
            } else if (!empty($latestSubscription && $latestSubscription->subscribable->paid_through == null)) {
                //calculate paid user details
                list($amount, $pStatus, $addPlan, $pMode, $notes) = $this->_calculatePaidUserDetails($latestSubscription);

                $start = config('services.plans_name.lifetime');
                $end = config('services.plans_name.lifetime');
            } else {
                $amount = null;
                $pStatus = config('services.payment_status.not_paid');
                $addPlan = true;
                $pMode = null;
                $notes = null;
                $start = null;
                $end = null;
            }

            $data[] = array(
                's_no' => $key + 1,
                'name' => $user->user->name,
                'email' => $user->user->email,
                'ip_address' => $user->user->ip_address,
                'role' => config('services.user_roles.' . $user->user->user_role),
                'start' => $start,
                'end' => $end,
                'mode' => $pMode,
                'amount' => $amount,
                'notes' => $notes,
                'discord_id' => $user->provider_user_id,
                'discord' => $user->nickname,
                'status' => $pStatus,
                'mix_data' => array(
                    'userId' => $user->user->id,
                    'email' => $user->user->email,
                    'role' => $user->user->user_role,
                    'name' => $user->user->name,
                    'add_plan' => $addPlan
                )
            );
        }

        return response()->json(['data' => $data]);
    }

    private function _calculatePaidUserDetails($latestSubscription)
    {
        //added subscription id, profile id, charge id along with notes
        if ($latestSubscription->subscribable_type == config('services.subscribable_type.stripe')) {
            $notes = $latestSubscription->notes . ' ' . $latestSubscription->subscribable->subscription_id;
        } else if ($latestSubscription->subscribable_type == config('services.subscribable_type.paypal')) {
            $notes = $latestSubscription->notes . ' ' . $latestSubscription->subscribable->profile_id;
        } else {
            $notes = $latestSubscription->notes;
        }

        //get how much user paid
        $paidAmount = (($latestSubscription->subscribable_type == config('services.subscribable_type.manual') || $latestSubscription->coupon_id != null))
            ? config('services.currency_symbol.dollar') . $latestSubscription->amount
            : config('services.currency_symbol.dollar') . $latestSubscription->plan->amount / 100;

        return array(
            $paidAmount,
            config('services.payment_status.paid'),
            false,
            config('services.subscribable_type_inverse.' . $latestSubscription->subscribable_type),
            $notes,
        );
    }

    /**
     * update user profile
     * @param Request $request
     * @return type
     */
    public function updateProfile(Request $request)
    {
        $allValues = $request->all();

        // Begin Transaction
        DB::beginTransaction();

        try {
            $updateUserTable = User::whereId($allValues['userid'])->update(['name' => $allValues['name'], 'email' => $allValues['email']]);
            $updateProviderTable = UserProviderAccount::whereUserId($allValues['userid'])->update(['name' => $allValues['name'], 'email' => $allValues['email']]);

            // Commit Transaction
            DB::commit();
            $success = true;
            $message = 'User updated successfully';
        } catch (\Exception $e) {
            // Rollback Transaction
            DB::rollback();
            $success = false;
            $message = 'Something went wrong';
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }

    /**
     * change user role
     * @param Request $request
     * @return type
     */
    public function updateRole(Request $request)
    {
        $allValues = $request->all();

        $updateRole = User::whereId($allValues['userid'])->update(['user_role' => $allValues['assignRole']]);

        if ($updateRole) {
            return response()->json(['success' => true, 'message' => 'User role updated successfully']);
        } else {
            return response()->json(['success' => false, 'message' => 'Something went wrong']);
        }
    }

    /**
     * add user plan
     * @param Request $request
     * @return type
     */
    public function addPlan(Request $request)
    {
        $allValues = $request->all();

        // Begin Transaction
        DB::beginTransaction();

        try {
            // add manual subscription
            $addSubscriptionManual = new SubscriptionManual;

            $addSubscriptionManual->status = config('services.status.paypal');
            $addSubscriptionManual->paid_through = Carbon::parse($allValues['end']);

            $addSubscriptionManual->save();

            // add subscription details
            $addSubscription = new Subscription;

            $addSubscription->subscribable_id = $addSubscriptionManual->id;
            $addSubscription->subscribable_type = config('services.subscribable_type.manual');
            $addSubscription->user_id = $allValues['userid'];
            $addSubscription->plan_id = $allValues['plan'];
            $addSubscription->amount = $allValues['amount'];
            $addSubscription->notes = $allValues['notes'];
            $addSubscription->status = config('services.status.paypal');

            $addSubscription->save();

            // Commit Transaction
            DB::commit();
            $success = true;
            $message = 'User updated successfully';
        } catch (\Exception $e) {
            // Rollback Transaction
            DB::rollback();
            $success = false;
            $message = 'Something went wrong';
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }

    public function fetchPlan()
    {
        return response()->json(['success' => true, 'data' => Plan::get()]);
    }

    /**
     * api for getting user transaction details
     */
    public function fetchTransactionsDetails($user_id)
    {
        $getTransactions = Subscription::with('user')->whereUserId($user_id)->get();

        $data = array();
        if (!empty($getTransactions)) {
            foreach ($getTransactions as $key => $transaction) {
                $userName = $transaction->user->name;

                $data[] = array(
                    's_no' => $key + 1,
                    'mode' => config('services.subscribable_type_inverse.' . $transaction->subscribable_type),
                    //check if subscription is manual
                    'amount' => $transaction->subscribable_type == config('services.subscribable_type.manual') ? config('services.currency_symbol.dollar') . $transaction->amount : config('services.currency_symbol.dollar') . $transaction->plan->amount / 100,
                    //check if subscription is for lifetime with paid_through null
                    'start_date' => $transaction->subscribable->paid_through == null ? config('services.plans_name.lifetime') : Carbon::parse($transaction->subscribable->created_at)->format('d M Y'),
                    'end_date' => $transaction->subscribable->paid_through == null ? config('services.plans_name.lifetime') : Carbon::parse($transaction->subscribable->paid_through)->format('d M Y'),
                );
            }
        }

        return response()->json(['user_name' => $userName, 'data' => $data]);
    }

    /**
     * add discount coupons
     * @param Request $request
     * @return type
     */
    public function addCoupon(Request $request)
    {
        $allValues = $request->all();

        $addCoupon = new DiscountCoupon;

        $addCoupon->coupon_name = $allValues['name'];
        $addCoupon->coupon_code = Str::random(8);
        $addCoupon->coupon_type = config('services.coupon_type.paypal');
        $addCoupon->max_redemptions = $allValues['max_redeemption'];
        $addCoupon->discount_percent = $allValues['discount'];
        $addCoupon->redeem_by = Carbon::parse($allValues['redeem_by']);

        if ($addCoupon->save()) {
            $success = true;
            $message = 'Coupon added successfully';
        } else {
            $success = false;
            $message = 'Something went wrong';
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }

    /**
     * fetch discount all coupons
     */
    public function fetchCoupons()
    {
        return response()->json(['data' => DiscountCoupon::whereCouponType(config('services.coupon_type.paypal'))->get()]);
    }
}
