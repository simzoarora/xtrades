<?php

namespace App\Http\Controllers;

use Socialite;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use App\User;
use App\UserProviderAccount;
use App\UserLog;

// $user = Socialite::driver('github')->userFromToken($token);

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function redirectToProvider($driver)
    {
        $driver = Socialite::driver($driver)->with(['auth_type' => 'rerequest']);

        if (method_exists($driver, 'asPopup')) {
            $driver->asPopup();
        }

        // // facebook
        // $driver->with(['auth_type' => 'rerequest']);

        // // discord 
        // $driver->with(['prompt' => 'consent']); 'none'

        return $driver->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $providerUser = Socialite::driver($provider)->user();

            return view('storage', [
                'storage' => [
                    'connect_token' => $providerUser->token,
                    'connect_provider' => $provider
                ]
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return view('storage');
        }
    }

    public function authWithProviderToken(Request $request, $provider)
    {
        $token = $request->get('token');
        $referralId = $request->get('referral_id');
        //check if session exist
        $paying_email = isset($_SESSION['paying_email']) ? $_SESSION['paying_email'] : null;

        $providerUser = Socialite::driver($provider)->userFromToken($token);

        $user = $this->createOrGetUser($providerUser, $provider, $referralId, $paying_email);
        $token = $this->guard()->login($user);

        $user->ip_address = $request->ip();
        $user->last_login_at = Carbon::now();
        $user->save();

        $this->guard()->setToken($token);

        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->getPayload()->get('exp') - time(),
        ];
    }

    public function createOrGetUser(ProviderUser $providerUser, $provider, $referralId, $paying_email)
    {
        $account = UserProviderAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new UserProviderAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider,
                'nickname' => $providerUser->getNickname(),
                'name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'avatar' => $providerUser->getAvatar()
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                if ($referralId != '' || $referralId != null) {

                    try {
                        $checkReferer = User::whereAffiliateId($referralId)->first();
                        $referralId = $checkReferer->id;

                    } catch (\Exception $e) {
                        $referralId = null;
                    }

                } else {
                    $referralId = null;
                }

                if ($paying_email != null) {
                    
                    // unset session
                    unset($_SESSION['paying_email']);

                    if ($paying_email == $providerUser->getEmail()) {
                        $user = User::whereEmail($paying_email)->first();
                    } else {
                        $user = User::whereEmail($paying_email)->first();

                        if ($user == null) {
                            $user = $this->_createUser($providerUser, $referralId);
                        } else {
                            $user->email = $providerUser->getEmail();
                            $user->name = $providerUser->getName();
                            $user->avatar = $providerUser->getAvatar();
                            $user->refer_id = $referralId;

                            $user->save();
                        }
                    }

                } else {
                    $user = $this->_createUser($providerUser, $referralId);
                }

                $checkLogs = UserLog::whereUserId($user->id)->whereSource(config('services.user_points.signup_points'))->first();

                if ($checkLogs == null && ($referralId != '' || $referralId != null)) { 
                    // providing signup points
                    $this->_signupPoints($checkReferer, $user, $referralId);
                }
            }

            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }

    /**
     * function for creating new user 
     *
     * @return void
     */
    private function _createUser($providerUser, $referralId)
    {
        return User::create([
            'email' => $providerUser->getEmail(),
            'name' => $providerUser->getName(),
            'avatar' => $providerUser->getAvatar(),
            'refer_id' => $referralId
        ]);
    }

    /**
     * function for assigning signup points
     *
     * @return void
     */
    private function _signupPoints($checkReferer, $user, $referralId)
    {

        // check if referral is not normal user 
        if ($checkReferer->user_role == config('services.user_roles_reverse.mentor')
            || $checkReferer->user_role == config('services.user_roles_reverse.marketer')) {
            $checkReferer->avail_points = $checkReferer->avail_points + config('services.user_points.signup_points');
            $checkReferer->save();

        //making logs of signup points
            $addDetails = new UserLog;

            $addDetails->user_id = $referralId;
            $addDetails->type = config('services.point_type.credit');
            $addDetails->source = config('services.point_source.signup');
            $addDetails->points = config('services.user_points.signup_points');
            $addDetails->refer_id = $user->id;

            $addDetails->save();
        }

        return true;

    }
}
