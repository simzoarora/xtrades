<?php

namespace App\Http\Controllers;

use Socialite;
use App\UserProviderAccount;
use Illuminate\Http\Request;

class ConnectionController extends Controller
{
    public function addConnection(Request $request, $driver)
    {
        $user = $request->user();
        $token = $request->get('token');
        $providerUser = Socialite::driver($driver)->userFromToken($token);
        $account = UserProviderAccount::whereProvider($driver)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            if ($user->is($account->user)) {
                return;
            }

            $account->delete();
        }

        $existingConnection = $user->connections()->where('provider', $driver)->first();
        if ($existingConnection) {
            $existingConnection->delete();
        }

        $account = new UserProviderAccount([
            'provider_user_id' => $providerUser->getId(),
            'provider' => $driver,
            'nickname' => $providerUser->getNickname(),
            'name' => $providerUser->getName(),
            'email' => $providerUser->getEmail(),
            'avatar' => $providerUser->getAvatar()
        ]);

        $account->user()->associate($user);
        $account->save();
    }
}
