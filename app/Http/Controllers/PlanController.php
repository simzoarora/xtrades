<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Stripe\Stripe;
use Stripe\Subscription as Stripe_Subscription;
use Stripe\Customer as Stripe_Customer;
use Stripe\Charge as Stripe_Charge;
use Stripe\Plan as Stripe_Plan;
use Stripe\Product as Stripe_Product;
use Stripe\Coupon as Stripe_Coupon;
use Stripe\Error\InvalidRequest;
use App\Services\DataAnalysisService;
use App\SubscriptionStripe;
use App\Subscription;
use App\PayPalService;
use App\SubscriptionPayPal;
use App\UserProviderAccount;
use App\DiscountCoupon;
use App\User;
use App\Plan;
use App\UserLog;
use App\FailedPayments;

class PlanController extends Controller
{

    use AuthenticatesUsers;

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function getExpressCheckoutUrl(Request $request, PayPalService $payPalService, Plan $plan)
    {
        return [
            'url' => $payPalService->getExpressCheckoutUrl($request->coupon, $request->refId, $plan)
        ];
    }

    public function doPayPal(Request $request, PayPalService $payPalService, Plan $plan, $coupon_id, $ref_id)
    {
        $validatedData = $request->validate([
            'token' => 'required',
            'PayerID' => 'required',
        ]);

        $token = $validatedData['token'];
        $payerId = $validatedData['PayerID'];

        /** @var SubscriptionPayPal $subscribable */
        $subscribable = $payPalService->processExpressCheckoutToken($plan, $token, $payerId, $coupon_id, $ref_id);

        $user = $subscribable->subscription->user;
        $token = $this->guard()->login($user);
        $this->guard()->setToken($token);

        return view('paypal-storage', [
            'storage' => [
                'token' => $token,
            ]
        ]);
    }

    public function doStripe(Request $request, Plan $plan)
    {
        $validatedData = $request->validate([
            'token' => 'required',
            'email' => 'required|email',
        ]);

        $user = $request->user();
        $product = $plan->product;
        $appliedCoupon = $request->coupon;

        $stripeProductId = "groupfish_product_{$product->id}";
        $stripePlanId = "groupfish_plan_{$plan->id}";
        $email = $validatedData['email'];
        $token = $validatedData['token'];
        $planId = $plan->id;

        Stripe::setApiKey(config('services.stripe.secret'));

        $options = [
            "stripe_account" => config('services.stripe.connected_account_id')
        ];

        /** @var Stripe_Customer $stripeCustomer */
        $stripeCustomer = Stripe_Customer::create([
            'source' => $token,
            'email' => $email
        ], $options);

        if ($plan->frequency != null && $plan->period != null) {

            try {
                $stripeProduct = Stripe_Product::retrieve($stripeProductId, $options);
            } catch (\Stripe\Exception\RateLimitException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\AuthenticationException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\ApiErrorException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (InvalidRequest $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);

                $stripeProduct = Stripe_Product::create([
                    "id" => $stripeProductId,
                    "name" => $product->name,
                    "type" => "service"
                ], $options);
            }

            try {
                $stripePlan = Stripe_Plan::retrieve($stripePlanId, $options);
            } catch (\Stripe\Exception\RateLimitException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\AuthenticationException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (\Stripe\Exception\ApiErrorException $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);
                return ['message' => $e->getJsonBody()['error']['message']];
            } catch (InvalidRequest $e) {
                Log::error('Email:- ' . $email . ', message:- ' . $e->getJsonBody()['error']['message']);

                $stripePlan = Stripe_Plan::create([
                    "nickname" => $plan->name,
                    'amount' => $plan->amount,
                    'interval' => $plan->period,
                    'interval_count' => $plan->frequency,
                    'id' => $stripePlanId,
                    'currency' => 'usd',
                    'product' => $stripeProductId,
                ], $options);
            }

            //if user applied any coupon
            if ($appliedCoupon != '' || $appliedCoupon != null) {
                /** @var Stripe_Subscription $stripeSubscription */
                $stripeSubscription = $stripeCustomer->subscriptions->create([
                    'plan' => $stripePlanId,
                    'coupon' => $appliedCoupon,
                ], $options);

            } else {
                /** @var Stripe_Subscription $stripeSubscription */
                $stripeSubscription = $stripeCustomer->subscriptions->create([
                    'plan' => $stripePlanId,
                        //'application_fee_percent' => 10
                ], $options);

            }

            $subscribable = SubscriptionStripe::create([
                'subscription_id' => $stripeSubscription->id,
                'status' => $stripeSubscription->status,
                'paid_through' => $stripeSubscription->current_period_end
            ]);

            $subscriptionTable = true;
        } else {
            //if user applied any coupon
            if ($appliedCoupon != '' || $appliedCoupon != null) {
                $getAmountDetails = $this->_calculateAmount($appliedCoupon, round($plan->amount / 100, 2));
                $payableAmount = $getAmountDetails['payableAmount'] * 100;
            } else {
                $payableAmount = $plan->amount;
            }


            try {
                //payment for lifetime plan
                $stripeCharge = Stripe_Charge::create([
                    "amount" => $payableAmount,
                    "currency" => "usd",
                    'customer' => $stripeCustomer->id,
                    "source" => $stripeCustomer->sources->data[0]->id, // obtained with Stripe.js
                    "description" => "Charge for " . $stripeCustomer->email
                ], $options);

                if ($stripeCharge->paid) {
                    $status = config('services.stripe_status.active');
                } else {
                    $status = config('services.stripe_status.pending');
                }

                $subscribable = SubscriptionStripe::create([
                    'subscription_id' => $stripeCharge->id,
                    'status' => $status,
                    'paid_through' => null
                ]);

                $subscriptionTable = true;
            } catch (InvalidRequest $e) {
                $status = config('services.stripe_status.pending');

                $subscriptionTable = false;
            } catch (\Exception $e) {
                $status = config('services.stripe_status.pending');

                $subscriptionTable = false;
            }
        }

        $email = $stripeCustomer->email;
        $name = $email;

        if (!$user) {
            $user = User::whereEmail($email)->first();

            if (!$user) {
                $gravatar = md5(strtolower($email));
                $user = User::create([
                    'email' => $email,
                    'name' => $name,
                    'avatar' => "https://www.gravatar.com/avatar/{$gravatar}.jpg?s=200&d=mm",
                    'password' => Hash::make(str_random(12)),
                ]);

                //set email in session 
                $_SESSION['paying_email'] = $user->email;

            }
        }

        if ($user && $user->refer_id != null) {
            if ($plan->period == '' || $plan->period == null) {
                $planPeriod = config('services.plans_name.lifetime');
            } else {
                $planPeriod = $plan->period;
            }

            $checkExist = User::whereId($user->refer_id)->first();

            //check if refId eexist and  user is paying using same person referral link
            if ($request->refId != null) {
                $checkExist = ($checkExist->affiliate_id == $request->refId)
                    ? $checkExist
                    : User::whereAffiliateId($request->refId)->first();
            }

            $givenPoints = 0;

            //check if referral user is normal user or mentor
            if ($checkExist->user_role == config('services.user_roles_reverse.user')) {
                $givenPoints = config('services.plans.' . $planPeriod . '');
                $checkExist->avail_points = $checkExist->avail_points + $givenPoints;
            } else if ($checkExist->user_role == config('services.user_roles_reverse.mentor')) {
                $givenPoints = config('services.user_points.payment_points');
                $checkExist->avail_points = $checkExist->avail_points + $givenPoints;
            }

            $checkExist->save();

            if ($givenPoints != 0) {
                //making logs of payment points
                $addDetails = new UserLog;

                $addDetails->user_id = $checkExist->id;
                $addDetails->type = config('services.point_type.credit');
                $addDetails->source = config('services.point_source.payment');
                $addDetails->points = $givenPoints;
                $addDetails->refer_id = $user->id;

                $addDetails->save();
            }
        }

        if ($request->refId && $request->refId != null) {
            // check if user is paying under same referral or another referral
            $user->payment_id = User::select('id')->whereAffiliateId($request->refId)->first()->id;
            $user->save();
        }

        if ($subscriptionTable) {
            $subscription = new Subscription();
            $subscription->plan()->associate($planId);
            $subscription->user()->associate($user);
            $subscription->status = $subscribable->status;

            //check if user applied a coupon
            if ($appliedCoupon != '' || $appliedCoupon != null) {
                $getAmountDetails = $this->_calculateAmount($appliedCoupon, round($plan->amount / 100, 2));
                $subscription->amount = $getAmountDetails['payableAmount'];
                $couponId = $getAmountDetails['couponId'];
            } else {
                $couponId = null;
            }

            $subscription->coupon_id = $couponId;
            $subscribable->subscription()->save($subscription);

            $token = $this->guard()->login($user);
            $this->guard()->setToken($token);

            $successMsg = $subscribable->status == config('services.stripe_status.incomplete') ? false : true;
        } else {
            $successMsg = false;
        }

        return [
            'token' => $token,
            'success' => $successMsg
        ];
    }

    private function _calculateAmount($appliedCoupon, $planAmnt)
    {
        $checkCoupon = DiscountCoupon::whereCouponCode($appliedCoupon)->first();

        return [
            'couponId' => $checkCoupon->id,
            'payableAmount' => $checkCoupon->discount_percent == null ? $planAmnt - $checkCoupon->discount_amount : $planAmnt - ($checkCoupon->discount_percent * $planAmnt / 100)
        ];
    }

    /**
     * api for getting user plans
     * @param type $discord_id
     * @return type
     */
    public function userPlan($discord_id)
    {
        $fetchType = UserProviderAccount::with('subscription.plan')
            ->providerDiscord()
            ->whereProviderUserId($discord_id)
            ->first();

        if ($fetchType != null && $fetchType->subscription != null && $fetchType->subscription->plan != null
            && ($fetchType->subscription->subscribable->paid_through > Carbon::now() || $fetchType->subscription->subscribable->paid_through == null)) {
            $success = true;
            $planName = $fetchType->subscription->plan->name;
            if ($fetchType->subscription->plan->period == null) {
                $period = config('services.plans_name.lifetime');
            } else {
                $period = $fetchType->subscription->plan->period;
            }
            $paid = config('services.plans_ordering.' . $period);
        } else {
            $success = false;
            $planName = null;
            $paid = 0;
        }

        return response()->json([
            'success' => $success,
            'user_plan' => $planName,
            'paid' => $paid
        ]);
    }

    /**
     * api for getting paid users details
     * @param type $discord_id
     * @return type
     */
    public function discordPaidUsers()
    {
        $fetchPaidUsers = UserProviderAccount::with(['latestSubscriptions' => function ($query) {
            $query->checkActivePayment()->with('plan');
        }])
            ->providerDiscord()
            ->get();

            // return response()->json($fetchPaidUsers->toArray());

        $paidData = array();

        foreach ($fetchPaidUsers as $discordPaid) {
            $latestSub = $discordPaid->latestSubscriptions;
            $period = 0;
            $previousPaid = 0;
            $changeId = null;

            if ($latestSub == null && $discordPaid->is_confirm != config('services.discord_response.confirmed')) {
                $changeId = $discordPaid->id;
            } else if ($latestSub != null && $latestSub->is_confirm != config('services.discord_response.confirmed')) {
                $changeId = $latestSub->id;
                //check plan period
                if ($latestSub->plan->period == null) {
                    $period = config('services.plans_name.lifetime');
                } else {
                    $period = $latestSub->plan->period;
                }

            //check if subscription not expired
                if ($latestSub->subscribable->paid_through > Carbon::now() || $latestSub->subscribable->paid_through == null) {
                    $period = config('services.plans_ordering.' . $period);

                //check if user current plan is active and another check if user has any previos plan
                    $checkPreviousPlan = Subscription::where('id', '!=', $latestSub->id)->whereUserId($latestSub->user_id)->checkActivePayment()->orderBy('created_at', 'desc')->first();
                
                //check if user paid for first time
                    if ($checkPreviousPlan == null) {
                        $previousPaid = 0;
                    } else {
                        $previousPeriod = $checkPreviousPlan->plan->period;
                        $previousPaid = config('services.plans_ordering.' . $previousPeriod);
                    }
                } else {
                    $previousPaid = config('services.plans_ordering.' . $period);
                    $period = 0;
                }
            }

            if ($changeId != null) {
                $paidData[] = array(
                    'change_id' => $changeId,
                    'discord_id' => $discordPaid->provider_user_id,
                    'previous_paid' => $previousPaid,
                    'current_paid' => $period
                );
            }
        }

        return response()->json($paidData);
    }

    /**
     * get id as parameter and then change it's status
     * @param type $changeId
     */
    public function confirmChange($changeId, $discordId)
    {
        $checkSubscription = UserProviderAccount::with(['latestSubscriptions' => function ($query) {
            $query->checkActivePayment();
        }])
            ->whereProviderUserId($discordId)
            ->providerDiscord()
            ->first();

        try {
            if ($checkSubscription->latestSubscriptions == null) {
                $updateConfirm = UserProviderAccount::whereId($changeId)->update(['is_confirm' => config('services.discord_response.confirmed')]);
            } else {
                $updateConfirm = Subscription::whereId($changeId)->update(['is_confirm' => config('services.discord_response.confirmed')]);
            }

            $success = true;
            $message = 'Status changed successfully';

        } catch (\Exception $e) {
            $success = false;
            $message = 'Something went wrong';
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }

    /**
     * fetch referral users
     * @return type json
     */
    public function fetchUsers(Request $request)
    {

        if ($request->days) {
            $days = $request->days;
        } else {
            $days = null;
        }

        $dataAnalys = new DataAnalysisService;

        $data = $dataAnalys->calculateAllValues($request->user()->id, $days);

        return response()->json($data);
    }

    public function fetchSubscriptions(Request $request)
    {
        $user = $request->user();
        $details = User::with('connections')
            ->with(['latestSubscriptions' => function ($query) {
                $query->checkActivePayment()->with('plan');
            }])
            ->where('refer_id', $user->id)
            ->get();
        $countPaid = 0;
        foreach ($details as $paid) {
            if ($paid->latestSubscriptions != null) {
                $countPaid += 1;
            }
        }

        return response()->json(['data' => $details, 'count_paid' => $countPaid]);
    }

    /**
     * check if stripe coupon exist
     * @param Request $request
     */
    public function checkCoupon(Request $request)
    {
        $couponCode = $request->coupon;

        Stripe::setApiKey(config('services.stripe.secret'));
            //check if coupon exist on stripe
        try {
            $getCoupon = Stripe_Coupon::retrieve($couponCode);
            
            //if coupon exist but not valid
            if ($getCoupon->valid === false) {
                return ['success' => false, 'message' => 'Coupon not valid'];
            } else {
                //check if coupon on our side
                $checkIfExist = DiscountCoupon::whereCouponCode($couponCode)->first();
 
                // save coupon details if not found
                if ($checkIfExist == null) {
                    $redeemBy = $getCoupon->redeem_by == null ? $getCoupon->redeem_by : Carbon::parse($getCoupon->redeem_by);

                    $addDetails = new DiscountCoupon;

                    $addDetails->coupon_name = $getCoupon->name;
                    $addDetails->coupon_code = $couponCode;
                    $addDetails->coupon_type = config('services.coupon_type.stripe');
                    $addDetails->max_redemptions = $getCoupon->max_redemptions;
                    $addDetails->discount_amount = $getCoupon->amount_off != null ? $getCoupon->amount_off / 100 : $getCoupon->amount_off;
                    $addDetails->discount_percent = $getCoupon->percent_off;
                    $addDetails->redeem_by = $redeemBy;

                    if (!$addDetails->save()) {
                        return ['success' => false, 'message' => 'Something went wrong'];
                    }
                } else {
                    $redeemBy = $checkIfExist->redeem_by;
                }

                if ($redeemBy != null && Carbon::now() > $redeemBy) {
                    return ['success' => false, 'message' => 'Coupon has been expired'];
                }
            }

        } catch (InvalidRequest $e) {
            return ['success' => false, 'message' => $e->getJsonBody()['error']['message']];
        }
    }

    public function checkCouponPaypal(Request $request)
    {
        $couponCode = $request->coupon;

        $checkIfExist = DiscountCoupon::whereCouponCode($couponCode)
            ->whereCouponType(config('services.coupon_type.paypal'))
            ->withCount('appliedSubscription')->first();
        
        // if coupon not found
        if ($checkIfExist == null) {
            return ['success' => false, 'message' => 'No such coupon: ' . $couponCode];
        } else {
            // if coupon maximum usage exceed
            if ($checkIfExist->applied_subscription_count >= $checkIfExist->max_redemptions ||
                Carbon::now() > $checkIfExist->redeem_by) {
                return ['success' => false, 'message' => 'Coupon not valid'];
            }
        }
    }

    public function chartData(Request $request)
    {
        //$request->user()->id
        $allUsers = User::with(['referrals.latestSubscriptions' => function ($query) {
            $query->whereStatus('active');
        }, 'referrals.connections', 'bonusEarned'])->whereId($request->user()->id)->first();

        $commisionPercent = "15%";
        $totalReferred = 0;
        $nextPercentAchieve = "18%";
        $totalActive = 0;
        $currentMonth = 0;
        $currentLevel = 1;
        $recurringAmount = 0;
        $bounsAmount = 0;
        $totalAmountAvg = 0;

        if (!empty($allUsers)) {
            $currentPoints = $allUsers->avail_points;
            $totalReferred = $allUsers->referrals->count();

            if ($currentPoints >= config('services.commision_rate.0.points') && $currentPoints < config('services.commision_rate.1.points')) {
                $commisionPercent = config('services.commision_rate.0.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.1.points') . '%';
                $currentLevel = config('services.commision_rate.0.level');

            } else if ($currentPoints >= config('services.commision_rate.1.points') && $currentPoints < config('services.commision_rate.2.points')) {
                $commisionPercent = config('services.commision_rate.1.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.2.points') . '%';
                $currentLevel = config('services.commision_rate.1.level');

            } else if ($currentPoints >= config('services.commision_rate.2.points') && $currentPoints < config('services.commision_rate.3.points')) {
                $commisionPercent = config('services.commision_rate.2.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.3.points') . '%';
                $currentLevel = config('services.commision_rate.2.level');

            } else if ($currentPoints >= config('services.commision_rate.3.points') && $currentPoints < config('services.commision_rate.4.points')) {
                $commisionPercent = config('services.commision_rate.3.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.4.points') . '%';
                $currentLevel = config('services.commision_rate.3.level');

            } else if ($currentPoints >= config('services.commision_rate.4.points') && $currentPoints < config('services.commision_rate.5.points')) {
                $commisionPercent = config('services.commision_rate.4.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.5.points') . '%';
                $currentLevel = config('services.commision_rate.4.level');

            } else if ($currentPoints >= config('services.commision_rate.5.points') && $currentPoints < config('services.commision_rate.6.points')) {
                $commisionPercent = config('services.commision_rate.5.percent');
                $nextPercentAchieve = ($allUsers->avail_points * 100) / config('services.commision_rate.6.points') . '%';
                $currentLevel = config('services.commision_rate.5.level');

            } else if ($currentPoints >= config('services.commision_rate.6.points')) {
                $commisionPercent = config('services.commision_rate.6.percent');
                $nextPercentAchieve = '100%';
                $currentLevel = config('services.commision_rate.6.level');

            }

            foreach ($allUsers->referrals as $checkSub) {
                if (Carbon::parse($checkSub->created_at)->month == Carbon::now()->month) {
                    $currentMonth += 1;
                }

                if (!empty($checkSub->latestSubscriptions)) {
                    //active users
                    if ($checkSub->latestSubscriptions->subscribable->paid_through > Carbon::now()) {
                        $totalActive += 1;
                    }
                }
            }
            
            //calculate bonus and earned amount            
            $to = Carbon::createFromFormat('Y-m-d H:s:i', $allUsers->created_at);
            $from = Carbon::createFromFormat('Y-m-d H:s:i', Carbon::now());
            $diff_in_months = $to->diffInMonths($from);

            foreach ($allUsers->bonusEarned as $getAmount) {
                if (Carbon::parse($getAmount->created_at)->month == Carbon::now()->month) {
                    $recurringAmount += $getAmount->earned;
                }

                if ($getAmount->bonus != null) {
                    $bounsAmount += $getAmount->bonus;
                }

                if ($getAmount->earned != null) {
                    $totalAmountAvg += $getAmount->earned;
                }
            }

            if ($diff_in_months != 0) {
                $totalAmountAvg = $totalAmountAvg / $diff_in_months;
            } else {
                $totalAmountAvg = $totalAmountAvg;
            }
        }

        return response()->json([
            'commision_percent' => $commisionPercent,
            'next_percent_achieve' => $nextPercentAchieve,
            'current_referrals' => $totalReferred,
            'current_month' => $currentMonth,
            'total_active' => $totalActive,
            'current_level' => $currentLevel,
            'chart_value' => 440,
            'total_level' => count(config('services.commision_rate')),
            'recurring_amount' => $recurringAmount,
            'total_amount_avg' => round($totalAmountAvg, 2),
            'bouns_amount' => $bounsAmount
        ]);
    }

    public function webHook(Request $request)
    {

        Log::info($request->type);

        if ($request->type == config('services.stripe_webhook.success') && $request->data['object']['object'] == config('services.stripe_webhook_type.invoice')) {
            $getDetails = $request->data['object']['lines']['data'][0];

            $subscription_id = $request->data['object']['subscription'];

            //check if hit is for subscription that is expired
            $checkifLatest = SubscriptionStripe::with('subscription')
                ->whereSubscriptionId($subscription_id)
                ->whereDate('paid_through', '<', Carbon::now())
                ->orderBy('created_at', 'desc')
                ->first();

            if ($checkifLatest != null) {

                //saving stripe details
                $addStripeSub = new SubscriptionStripe;

                $addStripeSub->subscription_id = $subscription_id;
                $addStripeSub->status = config('services.stripe_status.active');
                $addStripeSub->paid_through = Carbon::parse($getDetails['period']['end']);

                $addStripeSub->save();

                //saving subscription details
                $addSubscription = new Subscription;

                $addSubscription->subscribable_id = $addStripeSub->id;
                $addSubscription->subscribable_type = config('services.subscribable_type.stripe');
                $addSubscription->user_id = $checkifLatest->subscription->user_id;
                $addSubscription->plan_id = $checkifLatest->subscription->plan_id;
                $addSubscription->status = config('services.stripe_status.active');
                $addSubscription->is_confirm = config('services.discord_response.un_confirmed');

                $addSubscription->save();

            }
        } else if ($request->type == config('services.stripe_webhook.failed') && $request->data['object']['object'] == config('services.stripe_webhook_type.invoice')) {

            $subscription_id = $request->data['object']['subscription'];

            Log::info($subscription_id);

            sleep(5);

            Log::info('SLEEP DONE');

            //check if subscription exist
            $checkifExist = SubscriptionStripe::with('subscription')
                ->whereSubscriptionId($subscription_id)
                ->orderBy('created_at', 'desc')
                ->first();

            Log::info($checkifExist);

            if ($checkifExist != null) {
                $storeArray = $request->data['object'];

                $addFailedPayment = new FailedPayments;

                $addFailedPayment->subscription_id = $subscription_id;
                $addFailedPayment->user_id = $checkifExist->subscription->user_id;
                $addFailedPayment->customer_id = $storeArray['customer'];
                $addFailedPayment->invoice_id = $storeArray['id'];
                $addFailedPayment->amount_due = $storeArray['amount_due'];
                $addFailedPayment->amount_paid = $storeArray['amount_paid'];
                $addFailedPayment->amount_remaining = $storeArray['amount_remaining'];
                $addFailedPayment->type = config('services.payment_type.stripe');

                $addFailedPayment->save();

                Log::info($addFailedPayment);
            }
        } else if ($request->type == config('services.stripe_webhook.charge_failed')) {

            sleep(5);

            Log::info('SLEEP DONE');

            $storeArray = $request->data['object'];
            //check if subscription exist
            $checkifExist = User::whereEmail($storeArray['receipt_email'])->first();

            Log::info($checkifExist);

            if ($checkifExist != null) {
                $addFailedPayment = new FailedPayments;

                $addFailedPayment->subscription_id = $storeArray['id'];
                $addFailedPayment->user_id = $checkifExist->id;
                $addFailedPayment->customer_id = $storeArray['customer'];
                $addFailedPayment->invoice_id = $storeArray['id'];
                $addFailedPayment->amount_due = $storeArray['amount'];
                $addFailedPayment->amount_paid = 0.00;
                $addFailedPayment->amount_remaining = 0.00;
                $addFailedPayment->type = config('services.payment_type.stripe');

                $addFailedPayment->save();

                Log::info($addFailedPayment);
            }
        }
    }

    public function paypalWebHook(Request $request)
    {
        Log::info('Paypal:- ' . json_encode($request->all()));

        $allValues = $request->all();

        if ($allValues['txn_type'] == config('services.paypal_webhook.success')
            && $allValues['payment_status'] == config('services.status.paypal')) {
            //check if hit is for subscription that is expired
            $checkifExpired = SubscriptionPayPal::with('subscription')
                ->whereProfileId($allValues['recurring_payment_id'])
                ->whereDate('paid_through', '<', Carbon::now())
                ->orderBy('created_at', 'desc')
                ->first();

            if ($checkifExpired != null) {

                $paidThrough = $checkifExpired->subscription->plan->period == config('services.plans_name.month')
                    ? Carbon::now()->addMonth()
                    : Carbon::now()->addYear();

                //saving stripe details
                $addPaypalSub = new SubscriptionPayPal;

                $addPaypalSub->profile_id = $allValues['recurring_payment_id'];
                $addPaypalSub->status = $allValues['payment_status'];
                $addPaypalSub->paid_through = Carbon::parse($allValues['next_payment_date']);

                $addPaypalSub->save();
        
                //saving subscription details
                $addSubscription = new Subscription;

                $addSubscription->subscribable_id = $addPaypalSub->id;
                $addSubscription->subscribable_type = config('services.subscribable_type.paypal');
                $addSubscription->user_id = $checkifExpired->subscription->user_id;
                $addSubscription->plan_id = $checkifExpired->subscription->plan_id;
                $addSubscription->status = config('services.status.paypal');
                $addSubscription->is_confirm = config('services.discord_response.un_confirmed');

                $addSubscription->save();

            }
        } else if ($allValues['txn_type'] == config('services.paypal_webhook.failed')) {

            $subscription_id = $request->data['object']['subscription'];

            //check if subscription exist
            $checkifExist = SubscriptionPayPal::with('subscription')
                ->whereProfileId($allValues['recurring_payment_id'])
                ->orderBy('created_at', 'desc')
                ->first();

            if ($checkifExist != null) {

                $addFailedPayment = new FailedPayments;

                $addFailedPayment->subscription_id = $allValues['recurring_payment_id'];
                $addFailedPayment->user_id = $checkifExist->subscription->user_id;
                $addFailedPayment->customer_id = $allValues['payer_id'];
                $addFailedPayment->invoice_id = $allValues['receipt_id'];
                $addFailedPayment->amount_due = $allValues['mc_gross'];
                $addFailedPayment->amount_paid = 0.00;
                $addFailedPayment->amount_remaining = $allValues['outstanding_balance'];
                $addFailedPayment->type = config('services.payment_type.paypal');

                $addFailedPayment->save();
            }
        }

    }

    /**
     * reterive user referrals
     *
     * @return void
     */
    public function getReferrals()
    {

        $fetchReferrals = User::whereReferId(8)
            ->where('created_at', '<=', Carbon::now())
            ->where('created_at', '>=', Carbon::now()->subDays(6))
            ->get();

        foreach ($fetchReferrals as $referrals) {
            $joinedUser[] = array(
                strtotime($referrals->created_at) . '000',
                $referrals->id,
                $referrals->affiliate_id
            );
        }

        return response()->json($joinedUser);

    }

    /**
     * cancel subscription
     *
     * @return void
     */
    public function cancelSubscription(Request $request)
    {
        $subId = Subscription::whereUserId($request->user()->id)->orderBy('created_at', 'desc')->first();

        //check if subscription was paid from stripe or paypal
        if ($subId->subscribable_type == config('services.subscribable_type.stripe')) {
            Stripe::setApiKey(config('services.stripe.secret'));

            $cancelSubscription = Stripe_Subscription::retrieve($subId->subscribable->subscription_id);

            $checkCancelation = $cancelSubscription->cancel();

            //check if status cancel confirm
            if($checkCancelation->status == config('services.cancellation_status.canceled')) {
                $subId->status = $checkCancelation->status;
                $subId->save();

                SubscriptionStripe::whereId($subId->subscribable->id)->update(['status' => $checkCancelation->status]);
                
                $status = true;
                $msg = 'Subscription Cancelled';    
            } else {
                $status = false;
                $msg = 'Something went wrong';
            }

        } else {

        }

        return response()->json(['success' => $status, 'message' => $msg]);

    }

}
