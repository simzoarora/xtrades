<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;

class VerifyStripeSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $endpoint_secret = config('services.stripe.webhook_secret');

        //need to check same in laravel
        $payload = @file_get_contents('php://input');

        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            Log::info('STRIPE SIGNATURE VERIFICATION');
            $event = \Stripe\Webhook::constructEvent(
                $payload,
                $sig_header,
                $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
    // Invalid payload
    Log::error('PAYLOAD:-'. $e);
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
    Log::error('INVALID:-'. $e);
            http_response_code(400);
            exit();
        }

        return $next($request);
    }
}
