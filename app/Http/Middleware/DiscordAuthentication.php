<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class DiscordAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $breakToken = explode(' ', $request->header()['authorization'][0]);

        if ($breakToken[0] != env('CUSTOM_TOKEN_TYPE') || Hash::check(env('CUSTOM_TOKEN_VALUE'), $breakToken[1]) === false) {
            abort(404);
        }

        return $next($request);
    }
}
