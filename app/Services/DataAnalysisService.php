<?php

namespace App\Services;

use Carbon\Carbon;
use App\User;

class DataAnalysisService {

    public function __construct() {
        
    }

    /**
     * fetch leaderboard records
     */
    public function calculateAllValues($userId, $days) {
        $data = array();
        $makeQuery= User::with(['referrals.latestSubscriptions' => function($query) {
                            $query->checkActivePayment();
                        }, 'referrals.connections']);
        if ($userId == null) {
            $fetchUsers = $makeQuery->get();

            foreach ($fetchUsers as $users) {
                $getArray = $this->_makeAnalys($users);

                $data[] = array(
                    'name' => $getArray['name'],
                    'registrations' => $getArray['registrations'],
                    'unique' => $getArray['unique'],
                    'conversions' => $getArray['conversions'],
                    'active' => $getArray['active'],
                    'cancelled' => $getArray['cancelled'],
                    '% no discord' => $getArray['%_no_discord'],
                );
            }
        } else {
            
            //if no days are provided
            if($days == null) {
                $fetchUsers = $makeQuery->whereId($userId)->first();
            } else {
                $finalDate = $days = Carbon::now()->subDays($days);
                
                $fetchUsers = User::with(['referrals' => function($query) use($finalDate) {
                            $query->whereDate('created_at', '>=', $finalDate)
                                    ->with(['latestSubscriptions' => function($query) {
                                        $query->checkActivePayment();
                                    }]);
                        }, 'referrals.connections'])->whereId($userId)->first();
            }

            $getArray = $this->_makeAnalys($fetchUsers);

            $data[] = array(
                'name' => $getArray['name'],
                'registrations' => $getArray['registrations'],
                'unique' => $getArray['unique'],
                'conversions' => $getArray['conversions'],
                'active' => $getArray['active'],
                'cancelled' => $getArray['cancelled'],
                '%_no_discord' => $getArray['%_no_discord'],
            );
        }

        return $data;
    }

    private function _makeAnalys($users) {
        $totalUnique = 0;
        $totalUniquePercent = 0;
        $totalPaid = 0;
        $totalPaidPercent = 0;
        $totalActive = 0;
        $totalActivePercent = 0;
        $totalDiscord = 0;
        $totalDiscordPercent = 0;

        $totalReferral = $users->referrals->count();
        if ($totalReferral > 0) {
            $totalUnique = count(array_unique($users->referrals->pluck('ip_address')->toArray()));
            $totalUniquePercent = ($totalUnique * 100) / $totalReferral;

            //paid users
            foreach ($users->referrals as $checkSub) {
                if (!empty($checkSub->latestSubscriptions)) {
                    $totalPaid += 1;
                    //active users
                    if ($checkSub->latestSubscriptions->subscribable->paid_through > Carbon::now() || $checkSub->latestSubscriptions->subscribable->paid_through == null) {
                        $totalActive += 1;
                    }
                }
                //check discord users
                if (count($checkSub->connections) > 0) {
                    $totalDiscord += 1;
                }
            }

            //paid users percentage
            $totalPaidPercent = ($totalPaid * 100) / $totalReferral;
            //active users percentage
            $totalActivePercent = ($totalActive * 100) / $totalReferral;
            //discord users percentage
            $totalDiscordPercent = ($totalDiscord * 100) / $totalReferral;
        }

        return $data[] = array(
            'name' => $users->name,
            'registrations' => $totalReferral,
            'unique' => $totalUnique . ' (' . round($totalUniquePercent, 2) . '%)',
            'conversions' => $totalPaid . ' (' . round($totalPaidPercent, 2) . '%)',
            'active' => $totalActive . ' (' . round($totalActivePercent, 2) . '%)',
            'cancelled' => $totalActive . ' (' . round($totalActivePercent, 2) . '%)',
            '%_no_discord' => $totalDiscord . ' (' . round($totalDiscordPercent, 2) . '%)',
        );
    }

}
