<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPayPal extends Model
{
    use SoftDeletes;

    protected $table = 'subscriptions_paypal';
    protected $dates = ['paid_through'];
    protected $fillable = ['profile_id', 'status', 'paid_through'];

    public function subscription()
    {
        return $this->morphOne(Subscription::class, 'subscribable');
    }
}
