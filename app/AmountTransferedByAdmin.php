<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmountTransferedByAdmin extends Model
{
    protected $table = 'amount_transfered_by_admin';
}
