<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionManual extends Model
{
    protected $table = 'subscriptions_manual';
    protected $dates = ['paid_through'];
    protected $fillable = ['status', 'paid_through'];
    
    public function subscription()
    {
        return $this->morphOne(Subscription::class, 'subscribable');
    }
}
