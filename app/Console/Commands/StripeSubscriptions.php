<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;
use Stripe\Error\InvalidRequest;
use App\SubscriptionStripe;
use App\User;
use App\UserStripeSubscription;

class StripeSubscriptions extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create same subscriptions plan in new account for old customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        //set first account stripe key
        Stripe::setApiKey(config('services.stripe_test.key_1'));
        
        $previousPlans= \Stripe\Plan::all(["limit" => 50]);
        $custDetails= \Stripe\Customer::all(["limit" => 100]);
        
        $subscriptions= \Stripe\Subscription::all(['limit' => 50]);
        
        foreach($subscriptions->data as $sub) {
            $addDetails= new UserStripeSubscription;
            
            $addDetails->subscription_id= $sub->id;
            $addDetails->json_response= $sub;
            
            try {
                $addDetails->save();
            } catch (InvalidRequest $e) {
                Log::info('Subscription ID => '. $sub->id);
            }
        }
        
        //set second account stripe key
        Stripe::setApiKey(config('services.stripe_test.key_2'));
       
        //create plan
        foreach($previousPlans->data as $plan) {    
            try {
                $cPlan= \Stripe\Plan::create([
                    "amount" => $plan->amount,
                    "interval" => $plan->interval,
                    "interval_count" => $plan->interval_count,
                    "product" => [
                      "name" => $plan->nickname
                    ],
                    "currency" => $plan->currency,
                    "id" => $plan->id
                ]);
            } catch (InvalidRequest $e) {
                Log::info('Plan ID => '. $plan->id);
            }
        }
        
        //add subscription to customer
        foreach($custDetails->data as $singleCust) {
            
            foreach($singleCust->subscriptions->data as $subscription) {
                Stripe::setApiKey(config('services.stripe_test.key_1'));
                $invoice= \Stripe\Invoice::retrieve($subscription->latest_invoice);
                $checkDate= $invoice->created;
                $extendDate= strtotime(date("Y-m-d",strtotime('+1 month', $checkDate)));
                $days= round(($extendDate-time()) / (60 * 60 * 24));

                try {
                    Stripe::setApiKey(config('services.stripe_test.key_2'));
                    $subscriptionId= \Stripe\Subscription::create([
                        "customer" => $subscription->customer,
                        "items" => [
                          [
                            "plan" => $subscription->plan->id,
                          ],
                        ],
                        //"application_fee_percent" => 10,
                        "trial_end" => strtotime('+'.$days.' day', time()),
                    ]);
                    
                    //get user id
                    $fetchUser= SubscriptionStripe::with('subscription')->whereSubscriptionId($subscription->id)->first();
                    
                    if($fetchUser != NULL && $fetchUser->subscription != NULL) {
                        //update stripe id
                        User::whereId($fetchUser->subscription->user_id)->update(['stripe_customer_id' => $subscription->customer]);
                        
                        //update stripe subscription id
                        SubscriptionStripe::whereSubscriptionId($subscription->id)->update(['subscription_id' => $subscriptionId->id]);
                    }
                    
                } catch (InvalidRequest $e) {
                    Log::info('Customer ID => '. $singleCust->subscriptions->data[0]->customer);
                }
            }
        }
    }

}
