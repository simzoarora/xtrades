<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe\Stripe;
use Stripe\Error\InvalidRequest;
use App\UserStripeSubscription;

class CancelStripeSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancel:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel stripe subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //set first account stripe key
        Stripe::setApiKey(config('services.stripe_test.key_1'));
        
        $subscriptions= UserStripeSubscription::get();
        
        foreach ($subscriptions as $subscription) {
            $sub= \Stripe\Subscription::retrieve($subscription->subscription_id);
            
            try {
                $sub->cancel();
            } catch (InvalidRequest $e) {
                Log::info('Canceled Subscription ID => '. $subscription->subscription_id);
            }
        }
    }
}
