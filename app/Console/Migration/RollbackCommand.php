<?php
namespace App\Console\Migration;

use Illuminate\Database\Migrations\Migrator;
use Illuminate\Database\Console\Migrations\RollbackCommand as BaseRollbackCommand;

// tips: https://hackernoon.com/extending-laravels-migration-command-to-add-new-options-90b5a0fc4ef4

class RollbackCommand extends BaseRollbackCommand
{
    use MigrationTrait;

    public function __construct(Migrator $migrator)
    {
        parent::__construct($migrator);
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        // parent::handle();
        $this->runForAllGuilds();
    }
}
