<?php
namespace App\Console\Migration;

use App\Library\Venture;
use App\Guild;
use Illuminate\Support\Facades\DB;

trait MigrationTrait
{
    /**
     * Set schema.
     *
     * @param string $schema
     */
    protected function connectUsingDatabase(string $database)
    {
        $config = app('config');
        $config->set('database.connections.mysql.database', $database);
        DB::purge('mysql');
    }

    protected function runForAllGuilds()
    {
        $defaultDatabase = config('database.connections.mysql.database');

        foreach (Guild::GUILDS as $guild) {
            $this->comment("\nGuild: " . $guild['name']);
            $this->connectUsingDatabase($guild['db_database']);
            parent::handle();
        }

        $this->connectUsingDatabase($defaultDatabase);
    }
}
