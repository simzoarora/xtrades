<?php
namespace App\Providers;

use App\Console\Migration\MigrateCommand;
use App\Console\Migration\RollbackCommand;
use Illuminate\Database\MigrationServiceProvider;

class VentureMigrationServiceProvider extends MigrationServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
        $this->registerMigrateCommand();
        $this->registerMigrateRollbackCommand();
    }
    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerMigrateCommand()
    {
        $this->app->singleton('command.migrate', function ($app) {
            return new MigrateCommand($app['migrator']);
        });
    }

    protected function registerMigrateRollbackCommand()
    {
        $this->app->singleton('command.migrate.rollback', function ($app) {
            return new RollbackCommand($app['migrator']);
        });
    }
}
