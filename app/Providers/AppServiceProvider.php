<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Guild;
use App;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\SubscriptionPayPal;
use App\SubscriptionStripe;
use App\SubscriptionManual;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    { }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $guild = null;
        if (!app()->runningInConsole()) {

            if (config('app.debug') && request()->ip() !== '75.80.189.47') {
                abort(401);
            }

            $guild = collect(Guild::GUILDS)->firstWhere('tld', request()->getHost());
            if (!$guild) {
                Log::info(request()->all());
                abort(404);
            }

            $config = app('config');
            $config->set('guild', $guild);
            $config->set('app.name', $guild['app_name']);
            $config->set('app.url', $guild['app_url']);
            $config->set('app.icon', $guild['app_icon']);
            $config->set('app.logo', $guild['app_logo'] ?? '');
            $config->set('app.slogan', $guild['app_slogan'] ?? '');
            $config->set('database.connections.mysql.database', $guild['db_database']);
            $config->set('services.discord', $guild['discord']);
            $config->set('services.facebook', $guild['facebook']);
            $config->set('services.google', $guild['google']);
            $config->set('services.stripe', $guild['stripe']);
            $config->set('services.paypal', $guild['paypal']);
            $config->set('services.stripe_test', $guild['stripe_test']);
        } else {
            // $args = collect($_SERVER['argv'])->filter(function ($arg) {
            //     return stristr($arg, '=');
            // })->reduce(function ($carry, $arg) {
            //     list($var, $val) = explode('=', $arg);
            //     $carry[$var] = $val;
            //     return $carry;
            // }, []);

            // if (empty($args['--app'])) {
            //     dd($_SERVER['argv']);
            // }

            // $guild = collect(Guild::GUILDS)->firstWhere('name', $args['--app']);
        }

        Relation::morphMap([
            'subscription_paypal' => SubscriptionPayPal::class,
            'subscription_stripe' => SubscriptionStripe::class,
            'subscription_manual' => SubscriptionManual::class
        ]);
    }
}
