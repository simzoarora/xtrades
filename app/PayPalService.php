<?php

namespace App;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\DiscountCoupon;

class PayPalService
{

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    private function client()
    {
        $mode = config('services.paypal.mode');

        return new GuzzleClient([
            'base_uri' => $mode == 'production' ? 'https://api-3t.paypal.com' : 'https://api-3t.sandbox.paypal.com'
        ]);
    }

    public function post($params = [])
    {
        $params += [
            'requestEnvelope' => [
                'errorLanguage' => 'en_US',
                'detailLevel' => 'ReturnAll'
            ],
            'USER' => config('services.paypal.username'),
            'PWD' => config('services.paypal.password'),
            'SIGNATURE' => config('services.paypal.signature'),
            'VERSION' => 204
        ];

        $response = $this->client()->post('/nvp', [
            'form_params' => $params
        ]);

        $data = [];
        parse_str($response->getBody(), $data);

        if (empty($data['ACK']) || $data['ACK'] !== 'Success') {
            throw new \Exception('PayPal Request Error: ' . $data['L_SHORTMESSAGE0'] . ' (' . $data['L_LONGMESSAGE0'] . ')');
        }

        return $data;
    }

    public function processExpressCheckoutToken(Plan $plan, $token, $payerId, $coupon_id, $ref_id)
    {
        $expressCheckoutDetails = $this->post([
            'METHOD' => 'GetExpressCheckoutDetails',
            'TOKEN' => $token
        ]);

        $custom = json_decode($expressCheckoutDetails['CUSTOM'], true);

        $period = $custom['BILLINGPERIOD'];
        $frequency = $custom['BILLINGFREQUENCY'];
        $amount = $custom['AMT'];
        $cycles = $custom['TOTALBILLINGCYCLES'];

        $profileStartDate = Carbon::now();

        // we do INITAMT so don't actually do the first bill until 1 cycle passes
        // if it's a single cycle (1 time payment) then don't adjust so that the 1 cent goes ASAP
        if ($plan->frequency == null && $plan->period == null) {
            $custom['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
            $method = 'DoExpressCheckoutPayment';
        } else {
            $method = 'CreateRecurringPaymentsProfile';
            if ($cycles != 1) {
                switch (strtolower($period)) {
                    case 'year':
                        $profileStartDate->addYears($frequency);
                        break;
                    case 'month':
                        $profileStartDate->addMonths($frequency);
                        break;
                    case 'day':
                        $profileStartDate->addDays($frequency);
                        break;
                    default:
                        throw new \Exception('Bad subscription period!');
                }
            }
        }

        $data = $custom + [
            'METHOD' => $method,
            'TOKEN' => $token,
            'PAYERID' => $payerId,
            'PROFILESTARTDATE' => $profileStartDate->format('Y-m-d\TH:i:s\Z'),
            'CURRENCYCODE' => 'USD',
            'COUNTRYCODE' => 'US',
            'MAXFAILEDPAYMENTS' => 3,
            'FAILEDINITAMTACTION' => 'CancelOnFailure',
            'INITAMT' => $amount
        ];

        if ($cycles == 1) {
            // we can't drop this to 0 or it will run forever, so instead just charge 1 cent
            $data['INITAMT'] = $amount - 0.01;
            $data['AMT'] = 0.01;
        } else if ($data['TOTALBILLINGCYCLES'] > 1) {
            // drop this by 1 since we do INITAMT
            $data['TOTALBILLINGCYCLES'] -= 1;
        }

        $response = $this->post($data);

        if ($plan->frequency == null && $plan->period == null) {
            $profileDetails = $this->post([
                'METHOD' => 'GetExpressCheckoutDetails',
                'Token' => $response['TOKEN']
            ]);

            $name = $profileDetails['FIRSTNAME'] . ' ' . $profileDetails['LASTNAME'];
            $profileId = $profileDetails['TOKEN'];
            $status = $response['PAYMENTSTATUS'];
            $profileStartDate = null;
        } else {
            $profileId = $response['PROFILEID'];
            $profileDetails = $this->post([
                'METHOD' => 'GetRecurringPaymentsProfileDetails',
                'PROFILEID' => $profileId
            ]);

            $name = $profileDetails['SUBSCRIBERNAME'];
            $profileId = $profileDetails['PROFILEID'];
            $status = $profileDetails['STATUS'];
            $profileStartDate = Carbon::parse($profileDetails['PROFILESTARTDATE']);
        }

        // check if user is logged in, otherwise let's find a user with this same email otherwise make a new one
        $user = request()->user();

        $email = $expressCheckoutDetails['EMAIL'];

        if (!$user) {
            $user = User::whereEmail($email)->first();

            if (!$user) {
                $gravatar = md5(strtolower($email));
                $user = User::create([
                    'email' => $email,
                    'name' => $name,
                    'avatar' => "https://www.gravatar.com/avatar/{$gravatar}.jpg?s=200&d=mm",
                    'password' => Hash::make(str_random(12)),
                ]);

                //set email in session 
                $_SESSION['paying_email'] = $email;

            }
        }

        if ($user && $user->refer_id != null) {
            if ($plan->period == '' || $plan->period == null) {
                //config for accessing lifetime plan
                $planName = config('services.plans_ordering_reverse.3');
            } else {
                $planName = $plan->period;
            }
            $checkExist = User::whereId($user->refer_id)->first();

            //check if user is paying using another person referral link
            $checkExist = ($checkExist->affiliate_id == $request->refId)
                ? $checkExist
                : User::whereAffiliateId($request->refId)->first();

            $givenPoints = 0;

            //check if referral user is normal user or mentor
            if ($checkExist->user_role == config('services.user_roles_reverse.user')) {
                $givenPoints = config('services.plans.' . $planPeriod . '');
                $checkExist->avail_points = $checkExist->avail_points + $givenPoints;
            } else if ($checkExist->user_role == config('services.user_roles_reverse.mentor')) {
                $givenPoints = config('services.user_points.payment_points');
                $checkExist->avail_points = $checkExist->avail_points + $givenPoints;
            }

            $checkExist->save();

            if ($givenPoints != 0) {
            //making logs of payment points
                $addDetails = new UserLog;

                $addDetails->user_id = $checkExist->id;
                $addDetails->type = config('services.point_type.credit');
                $addDetails->source = config('services.point_source.payment');
                $addDetails->points = config('services.user_points.payment_points');
                $addDetails->refer_id = $user->id;

                $addDetails->save();
            }
        }

        if ($ref_id != 0) {
            // check if user is paying under same referral or another referral
            $user->payment_id = User::select('id')->whereAffiliateId($ref_id)->first()->id;
            $user->save();
        }

        $subscribable = SubscriptionPayPal::create([
            'profile_id' => $profileId,
            'status' => $status,
            'paid_through' => $profileStartDate
        ]);

        $subscription = new Subscription();
        $subscription->status = $subscribable->status;
        
        //check if user applied a coupon
        if ($coupon_id != 0) {
            $couponDetails = DiscountCoupon::whereId($coupon_id)->first();
            $amount = round($plan->amount / 100, 2);
            $subscription->amount = $amount - ($couponDetails->discount_percent * $amount / 100);
            $couponId = $couponDetails->id;
        } else {
            $couponId = null;
        }

        $subscription->coupon_id = $couponId;
        $subscription->plan()->associate($plan);
        $subscription->user()->associate($user);
        $subscribable->subscription()->save($subscription);

        return $subscribable;

        // dd([
        //     'res' => $response,
        //     'res2' => $response2,
        //     'res3' => $response3
        // ]);

        /*
        array:3 [▼
  "res" => array:39 [▼
    "TOKEN" => "EC-61119412UU0073544"
    "BILLINGAGREEMENTACCEPTEDSTATUS" => "1"
    "CHECKOUTSTATUS" => "PaymentActionNotInitiated"
    "TIMESTAMP" => "2019-07-12T06:51:36Z"
    "CORRELATIONID" => "ecab8562adc56"
    "ACK" => "Success"
    "VERSION" => "204"
    "BUILD" => "53232958"
    "EMAIL" => "tom-sl-viewer@gmail.com"
    "PAYERID" => "GUKTNXZUCGWSE"
    "PAYERSTATUS" => "verified"
    "FIRSTNAME" => "Tom"
    "LASTNAME" => "Maneri"
    "COUNTRYCODE" => "US"
    "ADDRESSSTATUS" => "Confirmed"
    "CURRENCYCODE" => "USD"
    "AMT" => "1.00"
    "ITEMAMT" => "1.00"
    "SHIPPINGAMT" => "0.00"
    "HANDLINGAMT" => "0.00"
    "TAXAMT" => "0.00"
    "CUSTOM" => "{"BILLINGPERIOD":"Month","BILLINGFREQUENCY":1,"AMT":1,"DESC":"description here","TOTALBILLINGCYCLES":0}"
    "DESC" => "description here"
    "INSURANCEAMT" => "0.00"
    "SHIPDISCAMT" => "0.00"
    "INSURANCEOPTIONOFFERED" => "false"
    "PAYMENTREQUEST_0_CURRENCYCODE" => "USD"
    "PAYMENTREQUEST_0_AMT" => "1.00"
    "PAYMENTREQUEST_0_ITEMAMT" => "1.00"
    "PAYMENTREQUEST_0_SHIPPINGAMT" => "0.00"
    "PAYMENTREQUEST_0_HANDLINGAMT" => "0.00"
    "PAYMENTREQUEST_0_TAXAMT" => "0.00"
    "PAYMENTREQUEST_0_CUSTOM" => "{"BILLINGPERIOD":"Month","BILLINGFREQUENCY":1,"AMT":1,"DESC":"description here","TOTALBILLINGCYCLES":0}"
    "PAYMENTREQUEST_0_DESC" => "description here"
    "PAYMENTREQUEST_0_INSURANCEAMT" => "0.00"
    "PAYMENTREQUEST_0_SHIPDISCAMT" => "0.00"
    "PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID" => "kevin-facilitator@enhancedinvestor.com"
    "PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED" => "false"
    "PAYMENTREQUESTINFO_0_ERRORCODE" => "0"
  ]
  "res2" => array:7 [▼
    "PROFILEID" => "I-K77XSVAHALXB"
    "PROFILESTATUS" => "PendingProfile"
    "TIMESTAMP" => "2019-07-12T06:51:40Z"
    "CORRELATIONID" => "8d763fc61a060"
    "ACK" => "Success"
    "VERSION" => "204"
    "BUILD" => "53072421"
  ]
  "res3" => array:35 [▼
    "PROFILEID" => "I-K77XSVAHALXB"
    "STATUS" => "Pending"
    "AUTOBILLOUTAMT" => "NoAutoBill"
    "DESC" => "description here"
    "MAXFAILEDPAYMENTS" => "3"
    "SUBSCRIBERNAME" => "Tom Maneri"
    "PROFILESTARTDATE" => "2019-08-12T07:00:00Z"
    "NUMCYCLESCOMPLETED" => "0"
    "NUMCYCLESREMAINING" => "0"
    "OUTSTANDINGBALANCE" => "0.00"
    "FAILEDPAYMENTCOUNT" => "0"
    "TRIALAMTPAID" => "0.00"
    "REGULARAMTPAID" => "0.00"
    "AGGREGATEAMT" => "0.00"
    "AGGREGATEOPTIONALAMT" => "0.00"
    "FINALPAYMENTDUEDATE" => "1970-01-01T00:00:00Z"
    "TIMESTAMP" => "2019-07-12T06:51:41Z"
    "CORRELATIONID" => "26e71902e21b"
    "ACK" => "Success"
    "VERSION" => "204"
    "BUILD" => "53072421"
    "BILLINGPERIOD" => "Month"
    "BILLINGFREQUENCY" => "1"
    "TOTALBILLINGCYCLES" => "0"
    "CURRENCYCODE" => "USD"
    "AMT" => "1.00"
    "SHIPPINGAMT" => "0.00"
    "TAXAMT" => "0.00"
    "REGULARBILLINGPERIOD" => "Month"
    "REGULARBILLINGFREQUENCY" => "1"
    "REGULARTOTALBILLINGCYCLES" => "0"
    "REGULARCURRENCYCODE" => "USD"
    "REGULARAMT" => "1.00"
    "REGULARSHIPPINGAMT" => "0.00"
    "REGULARTAXAMT" => "0.00"
  ]
]
         */

        // $subscribable = new PayPalSubscription([
        //     'profile_id' => $response['PROFILEID'],
        //     'status' => $response['STATUS'],
        //     'paid_through' => Carbon::parse($response['PROFILESTARTDATE'])
        // ]);

        // $subscribable->save();

        // $subscription = new Subscription();
        // $subscription->plan()->associate($plan);
        // $subscription->discount()->associate($discount);
        // $subscription->affiliate()->associate($affiliate);
        // $subscription->user()->associate($user);
        // $subscribable->subscription()->save($subscription);

        // $this->discordService->syncDiscordRolesForUser($user);

        // return true;
    }

    public function getExpressCheckoutUrl($coupon, $refId, Plan $plan)
    {
        $period = ucfirst($plan->period);
        $frequency = $plan->frequency;
        
        //if user applied a coupon
        if ($coupon != '' || $coupon != null) {
            $checkCoupon = DiscountCoupon::whereCouponCode($coupon)->first();
            $amount = round($plan->amount / 100, 2);
            $amount = $amount - ($checkCoupon->discount_percent * $amount / 100);
            $couponId = $checkCoupon->id;
        } else {
            $amount = round($plan->amount / 100, 2);
            $couponId = 0;
        }

        //if user is comming with referal link
        if ($refId != '' || $refId != null) {
            $refId = $refId;
        } else {
            $amount = round($plan->amount / 100, 2);
            $refId = 0;
        }

        $cycles = 0;
        $currency = 'USD';

        $description = $plan->name;

        $data = [
            'METHOD' => 'SetExpressCheckout',
            'BRANDNAME' => config('app.name'),
            'NOSHIPPING' => 1,
            'NONOTE' => 1,

            'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE',
            'PAYMENTREQUEST_0_AMT' => $amount,
            'PAYMENTREQUEST_0_ITEMAMT' => $amount,
            'PAYMENTREQUEST_0_CURRENCYCODE' => $currency,
            'PAYMENTREQUEST_0_DESC' => $description,
            'PAYMENTREQUEST_0_CUSTOM' => [
                'BILLINGPERIOD' => $period,
                'BILLINGFREQUENCY' => $frequency,
                'AMT' => $amount,
                'DESC' => $description,
                'TOTALBILLINGCYCLES' => $cycles
            ],

            'L_PAYMENTTYPE0' => 'InstantOnly',
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => $description,

            'CANCELURL' => config('app.url'),
            'RETURNURL' => url()->current() . '/' . $couponId . '/' . $refId . '/process'
        ];

        $data['PAYMENTREQUEST_0_CUSTOM'] = json_encode($data['PAYMENTREQUEST_0_CUSTOM']);

        $response = $this->post($data);

        $mode = config('services.paypal.mode');
        $base = $mode == 'production' ? 'https://www.paypal.com' : 'https://www.sandbox.paypal.com';

        return "{$base}/cgi-bin/webscr?cmd=_express-checkout&token={$response['TOKEN']}";
    }
}

/*
    public function apTest()
    {
        $client = new GuzzleClient([
            'base_uri' => 'https://svcs.sandbox.paypal.com',
        ]);

        $appId = 'APP-80W284485P519543T';

        $response = $client->post('/AdaptivePayments/Pay', [
            'headers' => [
                "X-PAYPAL-SECURITY-USERID" => config('services.paypal.username'),
                "X-PAYPAL-SECURITY-PASSWORD" => config('services.paypal.password'),
                "X-PAYPAL-SECURITY-SIGNATURE" => config('services.paypal.signature'),
                "X-PAYPAL-REQUEST-DATA-FORMAT" => "JSON",
                "X-PAYPAL-RESPONSE-DATA-FORMAT" => "JSON",
                "X-PAYPAL-APPLICATION-ID" => $appId
            ],
            'json' => [
                'actionType' => 'PAY',
                'receiverList' => [
                    'receiver' => [
                        [
                            'amount' => 1,
                            'email' => 'kevin-facilitator@enhancedinvestor.com'
                        ]
                    ]
                ],
                'currencyCode' => 'USD',
                'cancelUrl' => 'https://somesite.com',
                'returnUrl' => 'https://somesite.com/zz',
                'requestEnvelope' => [
                    'errorLanguage' => 'en_US',
                    'detailLevel' => 'ReturnAll'
                ],
            ]
        ]);

        return json_decode($response->getBody(), true);

        $data = [];
        parse_str($response->getBody(), $data);
        return $data;
    }
 */
