<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuildExample extends Model
{
  const GUILDS = [
    [
      'name' => 'xtrades',
      'app_name' => 'Xtrades',
      'tld' => 'xtrade.com',
      'app_url' => 'http://xtrade.com',
      'app_icon' => 'https://cdn.discordapp.com/avatars/542367341015597066/a1f8dd6766542b76a3f8880abf93cafc.png',
      'app_slogan' => '78% Success Rate Stock and Options Alerts',
      'join_chat_redirect' => 'https://discordapp.com/channels/542224582317441034/592838548332216322',
      'db_database' => 'xtrades',
      'discord' => [
        'client_id' => '576643748440834050',
        'client_secret' => 'N0wDArjNxZZzg0fn985ju1bD2EhNtMo1',
        'redirect' => 'https://members.xtrades.net/login/discord/callback'
      ],
      'google' => [
        'client_id' => '122654971847-f7s1a0agmjaujokfk7ug4d2a811f9vbb.apps.googleusercontent.com',
        'client_secret' => 'eNkYdMpOI-W-lafxlpewyZ7S',
        'redirect' => 'https://members.xtrades.net/login/google/callback'
      ],
      'facebook' => [
        'client_id' => '438679630244917',
        'client_secret' => '27271c1404d7f16c52f94880959cde00',
        'redirect' => 'https://members.xtrades.net/login/facebook/callback'
      ],
      'paypal' => [
        'mode' => 'production',
        'username' => 'alertssupernova_api1.gmail.com',
        'password' => 'T4SAQLKR4VLYFZ7W',
        'signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AVt17bRDuC9Xh2qE5SBmFCuo2t7T'
      ],
      'stripe' => [
        'key' => 'pk_test_M21hPzcxBGm8lI9WGjwWdiI300Xv2txmdc',
        'secret' => 'sk_test_NOYusVueDFtxvnAo7pwzanqz007LGZ1XRM',
        'webhook_secret' => '',
        'connected_account_id' => ''
      ],
      'stripe_test' => [
        'key_1' => 'sk_test_b0T7ZbEspBI03yrzFXIZnHGu00T3MYgaA9',
        'key_2' => 'sk_test_NOYusVueDFtxvnAo7pwzanqz007LGZ1XRM'
      ],
      'provider' => [
        'discord'  => 'discord',
        'google'   => 'google',
        'facebook' => 'facebook',
      ],
      'plans' => [
        'month'  => 2,
        'year'   => 5,
        'lifetime' => 6,
      ],
      'plans_ordering' => [
        'month'  => 1,
        'year'   => 2,
        'lifetime' => 3,
      ],
      'plans_ordering_reverse' => [
        '1'  => 'month',
        '2'   => 'year',
        '3' => 'lifetime',
      ],
      'commision_rate' => [
        0 => ['points' => '1',  'percent' => '15%', 'level' => 1],
        1 => ['points' => '25',  'percent' => '18%', 'level' => 2],
        2 => ['points' => '50',  'percent' => '21%', 'level' => 3],
        3 => ['points' => '100',  'percent' => '24%', 'level' => 4],
        4 => ['points' => '200',  'percent' => '27%', 'level' => 5],
        5 => ['points' => '400',  'percent' => '30%', 'level' => 6],
        6 => ['points' => '1000',  'percent' => '40%', 'level' => 7],
      ],
      'status' => [
        'stripe'  => 'active',
        'paypal'  => 'Completed'
      ],
    ],
    [
      'name' => 'cryptotraders',
      'app_name' => 'CryptoTraders',
      'tld' => 'members.cryptotraders.com',
      'app_url' => 'https://members.cryptotraders.com',
      'app_icon' => 'https://cdn.discordapp.com/icons/542225548982747147/239f180b0d1077eb2420e08f8ca51871.png',
      'app_logo' => '/img/ct-logo.png',
      'app_slogan' => 'A place for Crypto Traders to unite & find an edge together',
      'join_chat_redirect' => 'https://discordapp.com/channels/542225548982747147/542444733432332300',
      'db_database' => 'cryptotraders',
      'discord' => [
        'client_id' => '590799931787444244',
        'client_secret' => 't7WcvmvvMfRRzzS8l80874Kx6hiG461m',
        'redirect' => 'https://members.cryptotraders.com/login/discord/callback'
      ],
      'google' => [
        'client_id' => '122654971847-4ed7jstnkinjbcb2tfbh3apndbc2but4.apps.googleusercontent.com',
        'client_secret' => '8BpKcQK8aDPBaXU4kCF3RUFY',
        'redirect' => 'https://members.cryptotraders.com/login/google/callback'
      ],
      'facebook' => [
        'client_id' => '415838485677555',
        'client_secret' => '45e25932c42007f6658e993c3990a814',
        'redirect' => 'https://members.cryptotraders.com/login/facebook/callback'
      ],
      'paypal' => [
        'mode' => 'production',
        'username' => 'kevin_api1.cryptotraders.com',
        'password' => 'XREZRQGVZKMTCPTN',
        'signature' => 'AsLY2Drwwl7PaKj570g8-cOvjcdeAbsvjyQXd9vkSl9EUxoGfILtjq0R'
      ],
      'stripe' => [
        'key' => 'pk_test_M21hPzcxBGm8lI9WGjwWdiI300Xv2txmdc',
        'secret' => 'sk_test_NOYusVueDFtxvnAo7pwzanqz007LGZ1XRM',
        'webhook_secret' => '',
        'connected_account_id' => ''
      ],
      'stripe_test' => [
        'key_1' => 'sk_test_b0T7ZbEspBI03yrzFXIZnHGu00T3MYgaA9',
        'key_2' => 'sk_test_NOYusVueDFtxvnAo7pwzanqz007LGZ1XRM'
      ],
      'provider' => [
        'discord'  => 'discord',
        'google'   => 'google',
        'facebook' => 'facebook',
      ],
      'plans' => [
        'month'  => 2,
        'year'   => 5,
        'lifetime' => 6,
      ],
      'plans_ordering' => [
        'month'  => 1,
        'year'   => 2,
        'lifetime' => 3,
      ],
      'plans_ordering_reverse' => [
        '1'  => 'month',
        '2'   => 'year',
        '3' => 'lifetime',
      ],
      'commision_rate' => [
        0 => ['points' => '1',  'percent' => '15%', 'level' => 1],
        1 => ['points' => '25',  'percent' => '18%', 'level' => 2],
        2 => ['points' => '50',  'percent' => '21%', 'level' => 3],
        3 => ['points' => '100',  'percent' => '24%', 'level' => 4],
        4 => ['points' => '200',  'percent' => '27%', 'level' => 5],
        5 => ['points' => '400',  'percent' => '30%', 'level' => 6],
        6 => ['points' => '1000',  'percent' => '40%', 'level' => 7],
      ],
      'status' => [
        'stripe'  => 'active',
        'paypal'  => 'Completed'
      ],
    ]
  ];
}

/*

cryptotraders
{
    "access_token": "sk_live_JHjq23ZYdc8y9s1cVdTCMkzc00a6YiabCf",
    "livemode": true,
    "refresh_token": "rt_FMZIsHXI5Al3irjddWi0zJVqdakjDeXwpfytIo7Mt9eLFAft",
    "token_type": "bearer",
    "stripe_publishable_key": "pk_live_PpMDBLIH55SqNyMt5i5XkuKE00n773ispd",
    "stripe_user_id": "acct_1Erq4jFg0cwOeLRj",
    "scope": "read_write"
}

*/
