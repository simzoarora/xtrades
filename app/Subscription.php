<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;

    protected $with = ['subscribable', 'plan'];

    public function subscribable()
    {
        return $this->morphTo();
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function scopeCheckActivePayment($query)
    {
        return $query->whereStatus(config('services.status.stripe'))->orWhere('status', config('services.status.paypal'));
    }
    
    public function scopeConfirmed($query)
    {
        return $query->whereIsConfirm(config('services.discord_response.confirmed'));
    }
    
    public function scopeUnConfirmed($query)
    {
        return $query->whereIsConfirm(config('services.discord_response.un_confirmed'));
    }

    public function toArray()
    {
        $data = parent::toArray();
        foreach ($this->getDates() as $key) {
            $attr = $this->getAttribute($key);
            if ($attr) {
                $data[$key] = intval($attr->format('U'));
            }
        }

        $data['attributes'] = $data['subscribable'];
        $data['type'] = $data['subscribable_type'];

        unset($data['subscribable']);
        unset($data['subscribable_id']);
        unset($data['subscribable_type']);

        return $data;
    }
}
