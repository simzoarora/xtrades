<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProviderAccount extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'provider_user_id', 'provider', 'nickname', 'name', 'email', 'avatar'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription', 'user_id', 'user_id');
    }
    
    public function latestSubscriptions()
    {
        return $this->hasOne('App\Subscription', 'user_id', 'user_id')->latest();
    }
    
    public function scopeProviderDiscord($query)
    {
        return $query->whereProvider(config('services.provider.discord'));
    }

    public function scopeConfirmed($query)
    {
        return $query->whereIsConfirm(config('services.discord_response.confirmed'));
    }
    
    public function scopeUnConfirmed($query)
    {
        return $query->whereIsConfirm(config('services.discord_response.un_confirmed'));
    }
}
