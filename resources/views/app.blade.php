@php
$config = [
'app_name' => config('app.name'),
'app_url' => config('app.url'),
'app_icon' => config('app.icon'),
'app_logo' => config('app.logo'),
'app_slogan' => config('app.slogan'),
'locale' => $locale = app()->getLocale(),
'locales' => config('app.locales'),
'stripe_pk' => config('services.stripe.key')
// 'githubAuth' => config('services.github.client_id'),
];
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" value="{{ csrf_token() }}" />
  <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <link href="{{ mix('css/' . strtolower($config['app_name']) . '.css') }}" rel="stylesheet">

  <script src="https://checkout.stripe.com/checkout.js" defer></script>
  <script>
    window.config = @json($config);
  </script>
  <script src="{{ mix('js/app.js') }}" defer></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

  <!-- jQuery Modal    -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

  <!-- Bootstrap datatables    -->
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" /> -->
  <script src="https://code.highcharts.com/stock/highstock.js"></script>
  <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
  <script type="text/javascript" src="https://www.highcharts.com/samples/data/usdeur.js"></script>




</head>

<body>
  <div id="app">
    <div id="loading">
      <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>

</body>

</html>