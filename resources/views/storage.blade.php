<html>
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }}</title>
  
  <script>

    var d = new Date();
    d.setTime(d.getTime() + (24*60*60*1000));
    var expires = "expires="+ d.toUTCString();

    @isset ($storage)
    @foreach ($storage as $k => $v)
    //store in local storage
    localStorage.setItem(@json($k), @json($v));

    //store in cookie
    if(@json($k) == 'connect_token' || @json($k) == 'connect_provider') {
      document.cookie = @json($k) + "=" + @json($v) + ";" + expires + ";path=/";      
    }

    @endforeach
    @endisset

    if (window.opener && window.opener !== window) {
      window.close();
    } else {
      window.location = @json(config('app.url').'/register');
    }
  </script>
</head>
<body></body>
</html>

