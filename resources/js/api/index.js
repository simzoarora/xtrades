import store from "../store";
import axios from "axios";

const client = axios.create({
  baseURL: '/api'
});

client.interceptors.request.use(
  config => {
    return {
      ...config,
      headers: {
        ...(config.headers || {}),
        Authorization: `Bearer ${store.state.token}`
      }
    };
  },
  error => {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default client;
