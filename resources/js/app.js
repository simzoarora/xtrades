import Vue from "vue";
import Toasted from "vue-toasted";
import BootstrapVue from "bootstrap-vue";

import store from "./store";
import router from "./router";

Vue.use(Toasted);
Vue.use(BootstrapVue);

import VueAxios from "vue-axios";
import axios from "axios";
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

(async() => {
    await store.dispatch("refreshAuth");

    const app = new Vue({
        store,
        router,
        template: "<router-view></router-view>"
    });

    app.$mount("#app");
})();

// var nice = $("body").niceScroll({
//     preservenativescrolling: false,
//     cursorwidth: '8px',
//     cursorborder: 'none',
//     cursorborderradius: '0px',
//     cursorcolor: "#39CCDB",
//     autohidemode: false,
//     background: "#999999"
// });