import Vue from "vue";
import Vuex from "vuex";
import router from "../router";
import api from "../api";

Vue.use(Vuex);

const ME = "ME";
const TOKEN = "TOKEN";

export default new Vuex.Store({
  state: {
    loading: false,
    app_name: window.config.app_name,
    app_url: window.config.app_url,
    app_icon: window.config.app_icon,
    app_logo: window.config.app_logo,
    app_slogan: window.config.app_slogan,
    stripe_pk: window.config.stripe_pk,
    me: null,
    token: null,
    referral_id: localStorage.getItem("_rid"),
    adminEmail: "admin@admin.com"
  },
  getters: {
    referralUrl({ me, app_url }) {
      return me ? `${app_url}/join/${me.affiliate_id}` : null;
    },
    isMobileDevice() {
      return (
        typeof window.orientation !== "undefined" ||
        navigator.userAgent.indexOf("IEMobile") !== -1
      );
    }
  },
  actions: {
    async refreshMe({ state, dispatch }) {
      let { data } = await api.get("/users/@me");
      dispatch("setMe", data);
    },

    async tryLoginWithConnectToken({ state }) {
      var connectToken = localStorage.getItem("connect_token") || "";
      var connectProvider = localStorage.getItem("connect_provider") || "";

      localStorage.removeItem("connect_token");
      localStorage.removeItem("connect_provider");

      //if token not found in localstorage then check for cookies
      if (connectToken == "" && connectProvider == "") {
        var tCookie = "connect_token";
        var pCookie = "connect_provider";

        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(";");
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == " ") {
            c = c.substring(1);
          }
          if (c.indexOf(tCookie) == 0) {
            var cToken = c.substring(name.length, c.length);
            cToken = cToken.split("=");
            connectToken = cToken[1];
          } else if (c.indexOf(pCookie) == 0) {
            var cProvider = c.substring(name.length, c.length);
            cProvider = cProvider.split("=");
            connectProvider = cProvider[1];
          }
        }
      }

      let {
        data: { token }
      } = await api.post(`/auth/${connectProvider}`, {
        token: connectToken,
        referral_id: state.referral_id
      });

      localStorage.setItem("token", token);
      localStorage.removeItem("_rid");

      // if (localStorage.plans) {
      //   localStorage.removeItem("plans");
      //   var getPlan = localStorage.getItem("plan_slug");
      //   localStorage.removeItem("plan_slug");

      // window.location.href = "/plans/" + getPlan;
      // } else {
      window.location.reload(true);
      // }
    },

    async tryConnectWithConnectToken({ state, dispatch }) {
      const connectToken = localStorage.getItem("connect_token") || "";
      const connectProvider = localStorage.getItem("connect_provider") || "";
      localStorage.removeItem("connect_token");
      localStorage.removeItem("connect_provider");

      await api.post(`/users/@me/connections/${connectProvider}`, {
        token: connectToken
      });

      dispatch("refreshMe");
    },

    async refreshAuth({ commit, state, dispatch }) {
      const token = localStorage.getItem("token") || "";

      if (!token) {
        // dispatch("logout");
        return;
      } else {
        commit(TOKEN, token);
      }

      try {
        await dispatch("refreshMe");
      } catch (err) {
        console.error(err);
        dispatch("logout");
      }
    },
    logout({ commit, dispatch }) {
      localStorage.removeItem("token");
      dispatch("setMe", null);
      router.push("login");
    },
    setMe({ commit, dispatch }, user) {
      commit(ME, user);
    }
  },
  mutations: {
    [ME](state, user) {
      state.me = user;
    },
    [TOKEN](state, token) {
      state.token = token;
    }
  }
});
