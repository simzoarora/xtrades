export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
 
export function copyToClipboard(str) {
  const el = document.createElement("textarea");
  el.value = str;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
}

let popoutInterval = null;
let popout = null;

export async function tryExpressCheckout(url) {
  return new Promise((resolve, reject) => {
    function closeLoginPopout() {
      clearInterval(popoutInterval);
      if (popout && !popout.closed) {
        popout.close();
      }
    }

    async function loginPopoutClosed() {
      clearInterval(popoutInterval);
      resolve();
    }

    const width = 600;
    const height = 800;
    const left = (screen.width - width) / 2;
    const top = (screen.height - height) / 4;

    const params = {
      toolbar: "no",
      location: "no",
      directories: "no",
      status: "no",
      menubar: "no",
      scrollbars: "no",
      resizable: "no",
      copyhistory: "no",
      width,
      height,
      top,
      left
    };

    const featuresStr = Object.keys(params)
      .reduce((carry, key) => [...carry, `${key}=${params[key]}`], [])
      .join(",");

    closeLoginPopout();
    popoutInterval = setInterval(() => {
      if (popout && popout.closed) {
        loginPopoutClosed();
      }
    }, 100);

// alert(featuresStr);

    window.location.href = url;

    // window.open(url, "_blank");

    // popout = window.open(url, "express-checkout-popout", featuresStr);

    // var windowReference = window.open();

    // myService.getUrl().then(function(url) {
    //     windowReference.location = url;
    // });
  });
}

export async function tryAddConnection(provider) {
  return new Promise((resolve, reject) => {
    function closeLoginPopout() {
      clearInterval(popoutInterval);
      if (popout && !popout.closed) {
        popout.close();
      }
    }

    async function loginPopoutClosed() {
      clearInterval(popoutInterval);
      resolve();
    }

    const url = `/login/${provider}`;

    // if (this.isMobileDevice) {
    //   window.location = url;
    //   return;
    // }

    const width = 600;
    const height = 800;
    const left = (screen.width - width) / 2;
    const top = (screen.height - height) / 4;

    const params = {
      toolbar: "no",
      location: "no",
      directories: "no",
      status: "no",
      menubar: "no",
      scrollbars: "no",
      resizable: "no",
      copyhistory: "no",
      width,
      height,
      top,
      left
    };

    const featuresStr = Object.keys(params)
      .reduce((carry, key) => [...carry, `${key}=${params[key]}`], [])
      .join(",");

    closeLoginPopout();
    popoutInterval = setInterval(() => {
      if (popout && popout.closed) {
        loginPopoutClosed();
      }
    }, 100);

    localStorage.setItem("checkSubscription", 1); 

    popout = window.open(url, "login-popout", featuresStr);
  });
}
