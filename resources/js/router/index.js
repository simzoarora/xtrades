import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

Vue.use(VueRouter);

import { sync } from "vuex-router-sync";

//Layouts
import AppLayout from "../layouts/AppLayout";
import DashboardLayout from "../layouts/DashboardLayout";
import PlansLayout from "../layouts/PlansLayout";
import AdminLayout from "../layouts/AdminLayout.vue";
import AdminLayoutNav from "../layouts/AdminLayoutNav.vue";

//auth free templates
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import TermsPage from "../pages/TermsPage";
import PrivacyPage from "../pages/PrivacyPage";
import DiscordLoginPage from "../pages/DiscordLoginPage";

//auth templates
import HomePage from "../pages/HomePage";
import PlansPage from "../pages/PlansPage";
import AffiliatesPage from "../pages/AffiliatesPage";
import LeaderboardPage from "../pages/LeaderboardPage";
import ChatPage from "../pages/ChatPage";
import ProductPage from "../pages/ProductPage";

//admin templates
import AdminLogin from "../pages/AdminLogin.vue";
import LeaderBoard from "../pages/admin/LeaderBoard.vue";
import Charts from "../pages/admin/Charts.vue";
import DiscordUsers from "../pages/admin/DiscordUsers.vue";
import DiscountCoupons from "../pages/admin/DiscountCoupons.vue";

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: AppLayout,
      props: 0,
      children: [
        {
          path: "",
          component: DashboardLayout,
          meta: { auth: true },
          children: [
            { path: "", component: HomePage },
            { path: "/chat", component: ChatPage },
            { path: "/product", component: ProductPage },
            { path: "/affiliates", component: AffiliatesPage },
            { path: "/membership", component: HomePage },
            { path: "/settings", component: HomePage },
            { path: "/referral-leaderboard", component: LeaderboardPage },
            {
              path: "/planspage",
              component: PlansPage,
              props: true,
              meta: { auth: true }
            },
            {
              path: "/planspage/:plan_slug?",
              component: PlansPage,
              props: true,
              meta: { auth: true, plans: true }
            }
          ]
        },
        {
          path: "",
          component: PlansLayout,
          meta: { auth: false },
          children: [
            {
              path: "/plans",
              component: PlansPage,
              props: true,
              meta: { auth: false }
            },
            {
              path: "/product",
              component: ProductPage,
              props: true,
              meta: { auth: false }
            },
            {
              path: "/plans/:plan_slug?",
              component: PlansPage,
              props: true,
              meta: { auth: true, plans: true }
            }
          ]
        }
      ]
    },
    {
      path: "/admin",
      component: AdminLayout,
      meta: { auth: true },
      props: 1,
      children: [
        { path: "/", component: LeaderBoard, props: true },
        { path: "/admin/discord-users", component: DiscordUsers, props: true },
        { path: "/admin/charts", component: Charts, props: true },
        {
          path: "/admin/discount-coupons",
          component: DiscountCoupons,
          props: true
        }
      ]
    },
    { path: "/admin-login", component: AdminLogin, meta: { guest: true } },
    { path: "/login", component: LoginPage, meta: { guest: true } },
    { path: "/register", component: RegisterPage, meta: { guest: true } },
    { path: "/terms", component: TermsPage },
    { path: "/privacy", component: PrivacyPage },
    { path: "/discord-login", component: DiscordLoginPage }
  ]
});

router.beforeEach((to, from, next) => {

    //check if user did the payment and not loggedin
    if (localStorage.getItem("payment_done") && !store.state.me) {
        localStorage.removeItem("payment_done");
        return next({
          path: "/discord-login"
        });
    }
  
    if (to.matched.some(record => record.meta.auth)) {
      if (!store.state.me) {
          return next({
            path: "/login"
          });
      } else if(to.matched[0].props.default == 1 && (store.state.me.user_role == 0 
        || store.state.me.user_role == 3 || store.state.me.user_role == 4)) {
          return next({
            path: "/login"
          });
      } else if(to.matched[0].props.default == 0 && store.state.me.user_role == 1) {
          return next({
            path: "/admin"
          });
      } 
      // else if(store.state.me.user_role == 1) {
        
      //   return next({
      //     path: "/admin"
      //   });
      // }
    } else if (to.matched.some(record => record.meta.guest)) {
      if (store.state.me) {
        return next({
          path: "/"
        });
      }
    } else if(to.matched[0] == undefined) {
      // console.log(to.matched[0]);
      return next({
        path: "/"
      });
    }
  
    return next();
  });

// sync(store, router);

export default router;
