import VueHighcharts from "../components/VueHighcharts.vue";
import More from "highcharts/highcharts-more";
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import JQuery from "jquery";
let $ = JQuery;
$(document).ready(function() {
    $(function() {
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });

    //bar chart
    $(function() {
        var chart,
            merge = Highcharts.merge;
        $(document).ready(function() {
            var perShapeGradient = {
                x1: 0,
                y1: 0,
                x2: 1,
                y2: 0
            };
            var colors = Highcharts.getOptions().colors;
            colors = [
                {
                    linearGradient: perShapeGradient,
                    stops: [[0, "rgb(34, 195, 170)"], [1, "rgb(36, 199, 175)"]]
                },
                {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, "rgb(119, 190, 255)"],
                        [1, "rgb(109, 100, 245)"]
                    ]
                },
                {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, "rgb(160, 150, 255)"],
                        [1, "rgb(165, 150, 255)"]
                    ]
                },
                {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, "rgb(254, 229, 125)"],
                        [1, "rgb(264, 249, 145)"]
                    ]
                }
            ];
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: "barcontainer",
                    type: "column",
                    backgroundColor: "#021c33"
                },
                xAxis: {
                    categories: [
                        "Activity",
                        "Conversations",
                        "Invites",
                        "Alerts"
                    ],
                    labels: {
                        style: {
                            color: "#fff"
                        }
                    }
                },
                title: {
                    text: "",
                    style: {}
                },
                yAxis: {
                    gridLineColor: "#11293d ",
                    title: {
                        text: ""
                    }
                },

                plotOptions: {
                    series: {
                        pointWidth: "16",
                        borderWidth: "0",
                        borderRadius: "4"
                    }
                },
                series: [
                    {
                        name: name,
                        data: [
                            {
                                y: 1355.11,
                                color: colors[0]
                            },
                            {
                                y: 181.63,
                                color: colors[1]
                            },
                            {
                                y: 450.63,
                                color: colors[2]
                            },
                            {
                                y: 181.94,
                                color: colors[3]
                            }
                        ]
                    }
                ],

                tooltip: {
                    outside: true,
                    useHTML: true,
                    borderColor: "#233249",
                    borderRadius: 5,
                    backgroundColor: "rgba(35, 50, 73, 1)",
                    style: {
                        opacity: 1,
                        color: "#fff",
                        background: "rgba(35, 50, 73, 1)"
                    }
                },

                exporting: {
                    enabled: true
                },
                credits: { enabled: true },
                legend: { enabled: true }
            });
        });
    });

    //spline chart
    Highcharts.chart("container", {
        chart: {
            type: "spline",
            backgroundColor: "#021c33"
        },
        title: {
            text: "",
            style: {
                color: "#fff"
            }
        },
        subtitle: {
            text: "",
            style: {
                color: "#808d99"
            }
        },
        xAxis: {
            categories: [
                "25.07",
                "26.07",
                "27.07",
                "28.07",
                "29.07",
                "30.07",
                "31.07"
            ]
        },
        yAxis: {
            gridLineColor: "#11293d ",
            title: {
                text: "",
                style: {
                    color: "#fff"
                }
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            },
            series: {
                showInLegend: false,
                color: "#22c3aa"
            }
        },
        tooltip: {
            outside: true,
            useHTML: true,
            borderColor: "#233249",
            borderRadius: 5,
            backgroundColor: "rgba(35, 50, 73, 1)",
            style: {
                opacity: 1,
                color: "#fff",
                background: "rgba(35, 50, 73, 1)",
                fontSize: "12px"
            }
        },
        series: [
            {
                name: "",
                data: [0, 5, 0.5, 20.5, 18.4, 10.5, 25.2],
                style: {
                    color: "#fff"
                }
            }
        ]
    });

    Highcharts.chart("messages", {
        chart: {
            type: "spline",
            backgroundColor: "#021c33"
        },
        title: {
            text: "",
            style: {
                color: "#fff"
            }
        },
        subtitle: {
            text: "",
            style: {
                color: "#808d99"
            }
        },
        xAxis: {
            categories: [
                "25.07",
                "26.07",
                "27.07",
                "28.07",
                "29.07",
                "30.07",
                "31.07"
            ]
        },
        yAxis: {
            gridLineColor: "#11293d ",
            title: {
                text: "",
                style: {
                    color: "#fff"
                }
            }
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            },
            series: {
                color: "#22c3aa"
            }
        },
        tooltip: {
            outside: true,
            useHTML: true,
            borderColor: "#233249",
            borderRadius: 5,
            backgroundColor: "rgba(35, 50, 73, 1)",
            style: {
                opacity: 1,
                color: "#fff",
                background: "rgba(35, 50, 73, 1)",
                fontSize: "12px"
            }
        },
        series: [
            {
                showInLegend: false,
                name: "",
                data: [0, 5, 0.5, 10.5, 8.4, 1.5, 5.2],
                style: {
                    color: "#fff"
                }
            }
        ]
    });

    // Uncomment to style it like Apple Watch

    if (!Highcharts.theme) {
        Highcharts.setOptions({
            chart: {
                backgroundColor: "#021c33"
            },
            colors: ["#22c3aa", "#77beff", "#a096ff", "#0e99ff", "#f06170"],
            title: {
                style: {
                    color: "silver"
                }
            },
            tooltip: {
                outside: true,
                useHTML: true,
                backgroundColor: "rgba(246, 246, 246, 1)",
                style: {
                    opacity: 1,
                    color: "#393939",
                    background: "rgba(246, 246, 246, 1)"
                }
            }
        });
    }

    /**
     * In the chart render event, add icons on top of the circular shapes
     */
    function renderIcons() {
        // Move icon
        if (!this.series[0].icon) {
            this.series[0].icon = this.renderer
                .path([])
                .attr({
                    stroke: "#021c33",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "stroke-width": 10,
                    zIndex: 10
                })
                .add(this.series[2].group);
        }
        this.series[0].icon.translate(
            this.chartWidth / 2 - 10,
            this.plotHeight / 2 -
                this.series[0].points[0].shapeArgs.innerR -
                (this.series[0].points[0].shapeArgs.r -
                    this.series[0].points[0].shapeArgs.innerR) /
                    2
        );

        // Move icon
        if (!this.series[0].icon) {
            this.series[0].icon = this.renderer
                .path([])
                .attr({
                    stroke: "#021c33",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "stroke-width": 10,
                    zIndex: 10
                })
                .add(this.series[2].group);
        }
        this.series[0].icon.translate(
            this.chartWidth / 2 - 10,
            this.plotHeight / 2 -
                this.series[0].points[0].shapeArgs.innerR -
                (this.series[0].points[0].shapeArgs.r -
                    this.series[0].points[0].shapeArgs.innerR) /
                    2
        );

        // Move icon
        if (!this.series[0].icon) {
            this.series[0].icon = this.renderer
                .path([])
                .attr({
                    stroke: "#021c33",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "stroke-width": 10,
                    zIndex: 10
                })
                .add(this.series[2].group);
        }
        this.series[0].icon.translate(
            this.chartWidth / 2 - 10,
            this.plotHeight / 2 -
                this.series[0].points[0].shapeArgs.innerR -
                (this.series[0].points[0].shapeArgs.r -
                    this.series[0].points[0].shapeArgs.innerR) /
                    2
        );

        // Exercise icon
        if (!this.series[1].icon) {
            this.series[1].icon = this.renderer
                .path([])
                .attr({
                    stroke: "#021c33",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "stroke-width": 10,
                    zIndex: 10
                })
                .add(this.series[2].group);
        }
        this.series[1].icon.translate(
            this.chartWidth / 2 - 10,
            this.plotHeight / 2 -
                this.series[1].points[0].shapeArgs.innerR -
                (this.series[1].points[0].shapeArgs.r -
                    this.series[1].points[0].shapeArgs.innerR) /
                    2
        );

        // Stand icon
        if (!this.series[2].icon) {
            this.series[2].icon = this.renderer
                .path([])
                .attr({
                    stroke: "#021c33",
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "stroke-width": 10,
                    zIndex: 10
                })
                .add(this.series[2].group);
        }

        this.series[2].icon.translate(
            this.chartWidth / 2 - 10,
            this.plotHeight / 2 -
                this.series[2].points[0].shapeArgs.innerR -
                (this.series[2].points[0].shapeArgs.r -
                    this.series[2].points[0].shapeArgs.innerR) /
                    2
        );
    }
});
